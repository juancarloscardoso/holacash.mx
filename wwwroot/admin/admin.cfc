<cfcomponent>
    <cffunction name="init" hint="initialize">
        <cffile action="read" file="#expandpath('/admin/setup.json')#" variable="setup">
        <cfset this["setup"] = deserializeJSON(setup)>
        <cfreturn this>
    </cffunction>

    <cffunction name="import_TX_Core" hint="step1">
        <cfargument name="qmonth">
        <cfargument name="qyear">

        <cfset totalInserts = 0>

        <!--- Get merchants --->
        <cfquery dbtype="odbc" datasource="hola" name="merchants">
            select merchant_name, "MSID" as myID
            from merchants_merchantsubaccount m
            order by merchant_name asc
        </cfquery>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="tx">
            select tracing_id
            from TX_core 
            where month(date_created) = <cfqueryparam value="#arguments.qmonth#">
               and year(date_created) = <cfqueryparam value="#arguments.qyear#">
        </cfquery>

        <!--- Get Baseline Query --->
        <cfquery dbtype="odbc" datasource="hola" name="step1">
            select 
                 distinct t.tracing_id
                ,t.amount
                ,t.cash_transaction_email
                ,t.identity_user_id
                ,t.original_cash_transaction_id
                ,t.receiving_subaccount_id as merchant_ID
                ,t.is_for_local_merchant 
                ,t.submission_channel
                ,t.payment_method
            FROM api_cashtransaction t
            WHERE t.transaction_type = 'charge'
            AND t.transaction_status != 'pending'
            and EXTRACT(year FROM t.date_created at time zone 'cdt') = #arguments.qyear#
            and EXTRACT(month FROM t.date_created at time zone 'cdt') = #arguments.qmonth#
            and t.tracing_id NOT IN (<cfqueryparam value="#valuelist(tx.tracing_id)#" list="true">)
        </cfquery>

        <!--- Insert Locally (try in case a Dup exists to skip) --->
        <cfloop query="step1">
            <cfset merchant_name = "">
            <cfloop query="merchants">
                <cfif step1.merchant_id[step1.currentRow] eq merchants.myID[merchants.currentRow]>
                    <cfset merchant_name = merchants.merchant_name[merchants.currentRow]>
                </cfif>
            </cfloop>

            <!--- Insert Core --->
            <cftry>
                <cfquery dbtype="odbc" datasource="hc" name="inserta">
                insert into TX_core 
                (
                    tracing_id 
                    ,MSID
                    ,merchant_name
                    ,amount 
                    ,cash_transaction_email 
                    ,identity_user_id 
                    ,original_cash_transaction_id
                    ,is_for_local_merchant
                    ,submission_channel
                    ,payment_method
                )
                values 
                (
                    <cfqueryparam value="#tracing_id#">
                    ,<cfqueryparam value="#merchant_ID#">
                    ,<cfqueryparam value="#merchant_name#">
                    ,<cfqueryparam value="#amount#">
                    ,<cfqueryparam value="#cash_transaction_emaiL#">
                    ,<cfqueryparam value="#identity_user_id#">
                    ,<cfqueryparam value="#original_cash_transaction_id#">
                    ,<cfqueryparam value="#is_for_local_merchant#">
                    ,<cfqueryparam value="#submission_channel#">
                    ,<cfqueryparam value="#payment_method#">
                )
                </cfquery>
                <cfset totalInserts = totalInserts + 1>
                <cfcatch></cfcatch>
            </cftry>
        </cfloop>

        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_Date" hint="step2">

        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step2">
            select tracing_id
            from TX_core 
            where date_created is null
        </cfquery>

        <!--- Get First DateTime --->
        <cfloop query="step2">
            <cfquery dbtype="odbc" datasource="hola" name="firstTID" maxrows="1">
                select date_created at time zone 'cdt' as mydate
                from api_cashTransaction 
                where tracing_id = <cfqueryparam value="#tracing_id#">
                order by date_created asc
                limit 1
            </cfquery>

            <cftry>
                <cfquery dbtype="odbc" datasource="hc">
                    update tx_core 
                    set date_created = #createodbcdatetime(firstTID.mydate)#
                    where tracing_id = <cfqueryparam value="#tracing_id#">
                </cfquery>
                <cfset totalInserts = totalInserts + 1>
                <cfcatch></cfcatch>
            </cftry>

        </cfloop>
        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_Approve" hint="step3">

        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step3">
            select tracing_id
            from TX_core 
            where approved_declined is null
            order by date_created
        </cfquery>

        <cfloop query="step3">
            <!--- Check for Succesful outcome --->
            <cfquery dbtype="odbc" datasource="hola" name="ad" maxrows="1">
                select p.response_status, cash_transaction_id
                from api_openpaycardevent p, api_cashtransaction t
                where  p.tracing_id = t.tracing_id
                and p.tracing_id = <cfqueryparam value="#tracing_id#">
                and t.transaction_status = 'completed'
                and t.transaction_type = 'charge'
                and p.processor_event_type = 'authorization'
                and  (    (left(p.response_status,3) = '3DS')
                    OR (p.response_status = 'APPROVAL')
                    )
                order by p.date_created desc
            </cfquery>

            <!--- Evaluate based on recordcount --->
            <cfif ad.recordcount gt 0><cfset Approved_Declined = "A">
            <cfelse><cfset Approved_Declined = "D"></cfif>

            <cftry>
                <!--- Update table --->                
                <cfquery dbtype="odbc" datasource="hc">
                    update tx_core 
                    set approved_declined = <cfqueryparam value="#Approved_declined#">
                    where tracing_id = <cfqueryparam value="#tracing_id#">
                </cfquery>
                <cfset totalInserts = totalInserts + 1>
                <cfcatch></cfcatch>
            </cftry>
        </cfloop>

        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_Processor" hint="step4">

        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step4">
            select tracing_id
            from TX_core 
            where processor_id is null
            order by date_created
        </cfquery>

        <cfloop query="step4">
            <!--- check for last processor if any --->
            <cfquery dbtype="odbc" datasource="hola" name="lastP" maxrows="1">
                select processor_id
                from api_openpaycardevent p, api_cashtransaction t
                where  p.tracing_id = t.tracing_id
                and p.tracing_id = <cfqueryparam value="#tracing_id#">
                and t.transaction_type = 'charge'
                and p.processor_event_type = 'authorization'
                order by p.date_created desc
            </cfquery>

            <cfif lastP.recordcount eq 0><cfset processor_id = 0>
            <cfelse><cfset processor_id = lastP.processor_id></cfif>

            <cftry>
                <!--- Update table --->                
                <cfquery dbtype="odbc" datasource="hc">
                    update tx_core 
                    set processor_id = <cfqueryparam value="#processor_id#">
                    where tracing_id = <cfqueryparam value="#tracing_id#">
                </cfquery>
                <cfset totalInserts = totalInserts + 1>
                <cfcatch></cfcatch>
            </cftry>
        </cfloop>
    
        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_3DS" hint="step5">

        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step5">
            select tracing_id
            from TX_core 
            where triggered_3ds is null
            order by date_created
        </cfquery>

        <cfloop query="step5">
            <!--- Check for Succesful outcome --->
            <cfquery dbtype="odbc" datasource="hola" name="tds" maxrows="1">
                select p.response_status, cash_transaction_id
                from api_openpaycardevent p, api_cashtransaction t
                where  p.tracing_id = t.tracing_id
                and p.tracing_id = <cfqueryparam value="#tracing_id#">
                and  (left(p.response_status,3) = '3DS')
                order by p.date_created desc
            </cfquery>

            <!--- Evaluate based on recordcount --->
            <cfif tds.recordcount gt 0><cfset triggered_3DS = 1>
            <cfelse><cfset triggered_3ds = 0></cfif>

            <cftry>
                <!--- Update table --->                
                <cfquery dbtype="odbc" datasource="hc">
                    update tx_core 
                    set triggered_3DS = <cfqueryparam value="#Triggered_3DS#">
                    where tracing_id = <cfqueryparam value="#tracing_id#">
                </cfquery>
                <cfset totalInserts = totalInserts + 1>
                <cfcatch></cfcatch>
            </cftry>
        </cfloop>

        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_Eci" hint="step6">

        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step6">
            select tracing_id
            from TX_core 
            where mapped_eci is null
            order by date_created
        </cfquery>

        <cfloop query="step6">
            <!--- Check for Succesful outcome --->
            <cfquery dbtype="odbc" datasource="hola" name="eci" maxrows="1">
                select r.mapped_eci as eci
                from api_cashtransactioneciresponse r, api_cashtransaction t 
                where t."TID" = r.cash_transaction_id 
                and t.tracing_id = <cfqueryparam value="#tracing_id#">
                order by t.date_created desc
            </cfquery>


            <cfif eci.recordcount eq 0 or len(eci.eci) eq 0><cfset mapped_eci = 0>
            <cfelse><cfset mapped_eci = eci.eci></cfif>

            <cftry>
                <!--- Update table --->                
                <cfquery dbtype="odbc" datasource="hc">
                    update tx_core 
                    set mapped_eci = <cfqueryparam value="#mapped_eci#">
                    where tracing_id = <cfqueryparam value="#tracing_id#">
                </cfquery>
                <cfset totalInserts = totalInserts + 1>
                <cfcatch></cfcatch>
            </cftry>    
        </cfloop>            
        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_Bin" hint="step7">
        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step7">
            select tracing_id
            from TX_core 
            where card_number is null
            order by date_created
        </cfquery>

        <cfloop query="step7">
            <cfquery dbtype="odbc" datasource="hola" name="detail1" maxrows="1">
                select card_number,bank_name,card_type
                from api_openpaycardevent p, api_cashtransaction t
                where  p.tracing_id = t.tracing_id
                and p.tracing_id = <cfqueryparam value="#tracing_id#">
                and char_length(card_number) > 0
                and left(card_number,4) != 'tok_'
                order by p.date_created desc
            </cfquery>

            <cfset hasToken = 0>
            <cfif detail1.recordcount eq 0>
                <cfquery dbtype="odbc" datasource="hola" name="detail2" maxrows="1">
                    select i.card_number, i.bank_name, i.card_type
                    from api_openpaycardevent p, api_cashtransaction t, api_openpaycardinformation i
                    where  p.tracing_id = t.tracing_id
                    and p.tracing_id = <cfqueryparam value="#tracing_id#">
                    and i.cash_transaction_id = t.original_cash_transaction_id
                    order by i.date_created desc
                </cfquery>
                <cfset card_number = detail2.card_number>
                <cfset bank_name = detail2.bank_name>
                <cfset card_type = detail2.card_type>
                <cfif detail2.recordcount eq 1>
                    <cfset hasToken = 1>
                </cfif>
            <cfelse>
                <cfset card_number = detail1.card_number>
                <cfset bank_name = detail1.bank_name>
                <cfset card_type = detail1.card_type>
            </cfif>

            <cfif len(trim(bank_name)) eq 0 and len(trim(card_number)) gt 0>
                <cfquery dbtype="odbc" datasource="hola" name="detail3" maxrows="1">
                    select card_type, bank_name 
                    from api_cashbinlookup
                    where left(card_bin,6) = <cfqueryparam value="#left(trim(card_number),6)#">
                </cfquery>
                <cfset bank_name = detail3.bank_name>
                <cfset card_type = detail3.card_type>
            </cfif>


            <cftry>
                <!--- Update table --->                
                <cfquery dbtype="odbc" datasource="hc"> 
                    update tx_core 
                    set card_number = <cfqueryparam value="#left(trim(card_number),6)#">
                        ,bank_name = <cfqueryparam value="#bank_name#">
                        ,card_type = <cfqueryparam value="#card_type#">
                        ,hasToken = <cfqueryparam value="#hasToken#">
                    where tracing_id = <cfqueryparam value="#tracing_id#">
                </cfquery>
                <cfset totalInserts = totalInserts + 1>
                <cfcatch></cfcatch>
            </cftry>    
        </cfloop>            
        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_Decline" hint="step8">
        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step8">
            select tracing_id
            from TX_core 
            where cash_decline_code is null
            order by date_created
        </cfquery>

        <cfloop query="step8">
            <cfquery dbtype="odbc" datasource="hola" name="decline" maxrows="1">
                select processor_decline_code,cash_decline_code,processor_decline_message
                from api_openpaycardevent p, api_cashtransaction t
                where  p.tracing_id = t.tracing_id
                and p.tracing_id = <cfqueryparam value="#tracing_id#">
                and char_length(cash_decline_code) > 0
                order by p.date_created asc
            </cfquery>

                <!--- Update table --->                
                <cfquery dbtype="odbc" datasource="hc">
                    update tx_core 
                    set cash_decline_code = <cfqueryparam value="#decline.cash_decline_code#">
                        ,processor_decline_code = <cfqueryparam value="#decline.processor_decline_code#">
                        ,processor_decline_message = <cfqueryparam value="#decline.processor_decline_message#">
                    where tracing_id = <cfqueryparam value="#tracing_id#">
                </cfquery>
                <cfset totalInserts = totalInserts + 1>
        </cfloop>            
        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_MSI" hint="step9">
        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step9">
            select tracing_id
            from TX_core 
            where isMSI is null
            order by date_created
        </cfquery>

        <cfset MSI = 0>
        <cfloop query="step9">
            <cfquery dbtype="odbc" datasource="hola" name="isMSI">
                select * 
                from api_cashtransactionmsiinformation m, api_cashtransaction t 
                where m.cash_transaction_id = t."TID"
                and t.tracing_id = <cfqueryparam value="#tracing_id#">
            </cfquery>
            <cfif isMSI.recordcount gt 0>
                <cfset MSI = 1>
            </cfif>

            <!--- Update table --->                
            <cfquery dbtype="odbc" datasource="hc">
                update tx_core 
                set isMSI = <cfqueryparam value="#MSI#">
                where tracing_id = <cfqueryparam value="#tracing_id#">
            </cfquery>
            <cfset totalInserts = totalInserts + 1>

            
        </cfloop>

        <cfreturn totalInserts>
    </cffunction>

    <cffunction name="import_TX_Fraud" hint="step10">
        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step10">
            select tracing_id
            from TX_core 
            where fraud_success is null
            order by date_created
        </cfquery>

        <cfloop query="step10">
            <cfquery dbtype="odbc" datasource="hola" name="fraud" maxrows="1">
            select is_success 
            from api_fraudscoringevent
            where tracing_id = <cfqueryparam value="#tracing_id#">
            order by date_created desc
            </cfquery>

            <cfquery dbtype="odbc" datasource="hc">
                update tx_core 
                set fraud_success = <cfqueryparam value="#fraud.is_success#">
                where tracing_id = <cfqueryparam value="#tracing_id#">
            </cfquery>
            <cfset totalInserts = totalInserts + 1>

        </cfloop>
    </cffunction>

    <cffunction name="import_TX_Description" hint="step11">
        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step11">
            select tracing_id
            from TX_core 
            where description is null
            order by date_created
        </cfquery>

        <cfloop query="step11">
            <cfquery dbtype="odbc" datasource="hola" name="describe" maxrows="1">
            select description
            from api_openpaycardevent p, api_cashtransaction t
            where  p.tracing_id = t.tracing_id
            and p.tracing_id = '#tracing_id#'
            and length(description) > 0
            order by t.date_created desc
            </cfquery>

            <cfquery dbtype="odbc" datasource="hc">
                update tx_core 
                set description = <cfqueryparam value="#describe.description#">
                where tracing_id = <cfqueryparam value="#tracing_id#">
            </cfquery>
            <cfset totalInserts = totalInserts + 1>

        </cfloop>
        <cfreturn totalInserts>
    </cffunction>


    <cffunction name="import_TX_Response" hint="step12">
        <cfset totalInserts = 0>

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step12">
            select tracing_id
            from TX_core 
            where response_status is null
            order by date_created
        </cfquery>

        <cfloop query="step12">
            <cfquery dbtype="odbc" datasource="hola" name="rs" maxrows="1">
            select response_status
            from api_openpaycardevent p, api_cashtransaction t
            where  p.tracing_id = t.tracing_id
            and p.tracing_id = '#tracing_id#'
            and length(response_status) > 0
            order by t.date_created desc
            </cfquery>

            <cfquery dbtype="odbc" datasource="hc">
                update tx_core 
                set response_status = <cfqueryparam value="#rs.response_status#">
                where tracing_id = <cfqueryparam value="#tracing_id#">
            </cfquery>
            <cfset totalInserts = totalInserts + 1>

        </cfloop>
        <cfreturn totalInserts>
    </cffunction>


    <cffunction name="import_TX_refunds" hint="GLOBAL">

        <!--- Refunds --->
        <cfquery dbtype="odbc" datasource="hola" name="refund">
        select distinct(t.tracing_id) as myTrace
        from api_openpaycardevent p, api_cashtransaction t
        where  p.tracing_id = t.tracing_id
        and t.transaction_status = 'completed'
        and p.processor_event_type = 'refund_at_processor'
        </cfquery>

        <cfloop query="refund">
            <cfquery dbtype="odbc" datasource="hc">
                update tx_core 
                set hasRefund = <cfqueryparam value="1">
                where tracing_id = <cfqueryparam value="#myTrace#">
            </cfquery>
        </cfloop>
        <cfreturn refund.recordcount>
    </cffunction>


    <cffunction name="missingSteps" hint="used to determine what is missing (if GT 0)">

        <!--- Step2 missing date --->
        <cfquery dbtype="odbc" datasource="hc" name="step2">
            select count(tracing_id) as total
            from TX_core 
            where date_created is null
        </cfquery>
        <cfif step2.total gt 0><cfreturn "step2:#step2.total#"></cfif>        

        <!--- Step3 missing approve --->
        <cfquery dbtype="odbc" datasource="hc" name="step3">
            select count(tracing_id) as total
            from TX_core 
            where approved_declined is null
        </cfquery>
        <cfif step3.total gt 0><cfreturn "step3:#step3.total#"></cfif>        

        <!--- Step4 missing processor ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step4">
            select count(tracing_id) as total
            from TX_core 
            where processor_id is null
        </cfquery>
        <cfif step4.total gt 0><cfreturn "step4:#step4.total#"></cfif>        

        <!--- Step5 missing triggered 3DS ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step5">
            select count(tracing_id) as total
            from TX_core 
            where triggered_3DS is null
        </cfquery>
        <cfif step5.total gt 0><cfreturn "step5:#step5.total#"></cfif>        

        <!--- Step6 missing Mapped ECI ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step6">
            select count(tracing_id) as total
            from TX_core 
            where mapped_eci is null
        </cfquery>
        <cfif step6.total gt 0><cfreturn "step6:#step6.total#"></cfif>        

        <!--- Step7 missing Card BIN --->
        <cfquery dbtype="odbc" datasource="hc" name="step7">
            select count(tracing_id) as total
            from TX_core 
            where card_number is null
        </cfquery>
        <cfif step7.total gt 0><cfreturn "step7:#step7.total#"></cfif>        

        <!--- Step8 missing Decline Code --->
        <cfquery dbtype="odbc" datasource="hc" name="step8">
            select count(tracing_id) as total
            from TX_core 
            where cash_decline_code is null
        </cfquery>
        <cfif step8.total gt 0><cfreturn "step8:#step8.total#"></cfif>        

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step9">
            select count(tracing_id) as total
            from TX_core 
            where isMSI is null
        </cfquery>
        <cfif step9.total gt 0><cfreturn "step9:#step9.total#"></cfif>        

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step10">
            select count(tracing_id) as total
            from TX_core 
            where fraud_success is null
        </cfquery>
        <cfif step10.total gt 0><cfreturn "step10:#step10.total#"></cfif>        

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step11">
            select count(tracing_id) as total
            from TX_core 
            where description is null
        </cfquery>
        <cfif step11.total gt 0><cfreturn "step11:#step11.total#"></cfif>        

        <!--- Get Local Existing Transactions to avoid Dups ---> 
        <cfquery dbtype="odbc" datasource="hc" name="step12">
            select count(tracing_id) as total
            from TX_core 
            where response_status is null
        </cfquery>
        <cfif step12.total gt 0><cfreturn "step12:#step12.total#"></cfif>        


        <cfreturn "step1:0">
    </cffunction>

    <cffunction name="dataSyncPercent" hint="returns a percent of data synch completeness">
    
        <cfquery dbtype="odbc" datasource="hc" name="s1">
        select count(*) as total from tx_core where len(date_created) = 0
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s2">
        select count(*) as total from tx_core where len(approved_declined) = 0
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s3">
        select count(*) as total from tx_core where len(processor_id) = 0
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s4">
        select count(*) as total from tx_core where len(triggered_3DS) = 0
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s5">
        select count(*) as total from tx_core where len(card_number) = 0
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s6">
        select count(*) as total from tx_core where len(isMSI) = 0
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s7">
        select count(*) as total from tx_core where len(fraud_success) = 0
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s8">
        select count(*) as total from tx_core where cash_decline_code is null
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s8">
        select count(*) as total from tx_core where mapped_eci is null
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s9">
        select count(*) as total from tx_core where description is null
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="s10">
        select count(*) as total from tx_core where response_status is null
        </cfquery>

        <cfquery dbtype="odbc" datasource="hc" name="all">
        select count(*) as total from tx_core
        </cfquery>

        <cfset TotalNulls = (s1.total + s2.total + s3.total + s4.total + s5.total + s6.total + s7.total + s8.total + s9.total + s10.total)>
        <cfset TotalNotNUlls = all.total*10>

        <cfset PercentComplete = numberformat((1-(TotalNulls/TotalNotNulls))*100,'0.000') & "%">
        <cfreturn PercentComplete>

    </cffunction>

    <cffunction name="isDataReady" hint="used to know if data has finished sync">
        <cfargument name="tracing_id">

        <cfquery dbtype="odbc" datasource="hc" name="checa">
            select *
            from TX_Core 
            where tracing_id = <cfqueryparam value="#tracing_id#">
        </cfquery>

        <cfif checa.recordcount eq 0><cfreturn false></cfif>
        <cfif len(checa.date_created) eq 0><cfreturn false></cfif>
        <cfif len(checa.approved_declined) eq 0><cfreturn false></cfif>
        <cfif len(checa.processor_id) eq 0><cfreturn false></cfif>
        <cfif len(checa.triggered_3DS) eq 0><cfreturn false></cfif>
        <cfif len(checa.card_number) eq 0><cfreturn false></cfif>
        <cfif len(checa.isMSI) eq 0><cfreturn false></cfif>
        <cfif len(checa.fraud_success) eq 0><cfreturn false></cfif>

        <cfif isnull(checa.cash_decline_code)><cfreturn false></cfif>
        <cfif isnull(checa.mapped_eci)><cfreturn false></cfif>
        <cfif isnull(checa.description)><cfreturn false></cfif>
        <cfif isnull(checa.response_status)><cfreturn false></cfif>

        <cfreturn true>
    </cffunction>

    <cffunction name="get_nonNetCashErrorCodes" hint="get Codes from JSON">
        <cfargument name="mode" default="Array" hint="array or list">
        <cfif arguments.mode eq 'List'>
            <cfreturn arraytolist(this.setup.nonNetCashErrorCodes)>
        <cfelse>
            <cfreturn this.setup.nonNetCashErrorCodes>
        </cfif>
    </cffunction>

    <cffunction name="get_testMerchants" hint="get Merchants from JSON">
        <cfargument name="mode" default="Array" hint="array or list">
        <cfif arguments.mode eq 'List'>
            <cfreturn arraytolist(this.setup.testMerchants)>
        <cfelse>
            <cfreturn this.setup.testMerchants>
        </cfif>
    </cffunction>

    <cffunction name="getAll" hint="Used for Summary">
        <cfargument name="onlyNet" default="false">
        <cfargument name="orderby" default="DESC">
        <cfargument name="maxrows" default="10000">
        <cfargument name="is_for_local_merchant" default="false" required="false">
        <cfargument name="payment_method" default="card" required="false">

        <!--- Error codes that SHOULD NOT be considered as NET or Attributable to HolaCash (ie No Funds) --->
        <cfset NonNetCashError = this.setup.nonNetCashErrorCodes>

        <cfquery dbtype="odbc" datasource="#Application.datasource#" name="all" maxrows="#arguments.maxrows#">
            select top #arguments.maxrows# *
            from TX_core 
            where tracing_id is not null
            <cfif onlyNet>
                and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
            </cfif>
            and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
            and payment_method = <cfqueryparam value="#arguments.payment_method#">
            order by date_created #arguments.orderby#
        </cfquery>
        <cfreturn all>
    </cffunction>

    <cffunction name="getTX" hint="Used for Detail">
        <cfargument name="tracing_id">
        <cfargument name="scopes" required="false" default="core,hola,request,trace,status,fraud,cashout">

        <cfset result = structnew()>

        <cfif listfindnocase(arguments.scopes,"core")>
            <cfquery dbtype="odbc" datasource="#application.datasource#" name="data1">
                select * from TX_core where tracing_id = <cfqueryparam value="#arguments.tracing_id#">
            </cfquery>
            <cfset result["core"] = data1>
        </cfif>

        <cfif listfindnocase(arguments.scopes,"hola")>
            <cfquery dbtype="odbc" datasource="hola" name="data2">
                select *
                from api_openpaycardevent p, api_cashtransaction t
                where  p.tracing_id = t.tracing_id
                and p.tracing_id = <cfqueryparam value="#tracing_id#">
                and t.transaction_type = 'charge'
                order by p.processor_id, p.date_created asc
            </cfquery>
            <cfset result["hola"] = data2>
        </cfif>

        <cfif listfindnocase(arguments.scopes,"request")>
            <cftry>
                <cfquery dbtype="odbc" datasource="hola" name="data3" maxrows="1">
                    select detail
                    from api_cashcommerceapidetail
                    where cash_transaction_id = '#data1.original_cash_transaction_id#'
                    where length(detail) > 0
                    order by date_created asc
                </cfquery>
                <cfset result["request"] = data3.detail>
                <cfcatch></cfcatch>
            </cftry>
        </cfif>

        <cfif listfindnocase(arguments.scopes,"trace")>
            <cfquery dbtype="odbc" datasource="hola" name="data4">
                select trace
                from api_cashaggregatetrace            
                where tracing_id = <cfqueryparam value="#tracing_id#">
            </cfquery>
            <cfset result["trace"] = deserializeJSON(data4.trace)>
        </cfif>

        <cfif listfindnocase(arguments.scopes,"status")>
            <cfif len(data1.original_cash_transaction_id) gt 0>
                <cfquery dbtype="odbc" datasource="hola" name="data5">
                    select *
                    from api_transactionstatus
                    where cash_transaction_id = '#data1.original_cash_transaction_id#'
                </cfquery>
                <cfset result["status"] = data5>
            </cfif>
        </cfif>

        <cfif listfindnocase(arguments.scopes,"fraud")>
            <cfquery dbtype="odbc" datasource="hola" name="data6" maxrows="1">
                select fraud_rule_context
                from api_fraudrecord
                where tracing_id = <cfqueryparam value="#tracing_id#">
                order by date_created desc
            </cfquery>
            <cfset result["fraud"] = data6.fraud_rule_context>
        </cfif>

        <cfif listfindnocase(arguments.scopes,"cashout")>
            <cftry>
                <cfquery dbtype="odbc" datasource="hola" name="data7">
                    select *
                    from api_cashoutspeiinformation
                    where cash_transaction_id = '#data1.original_cash_transaction_id#'
                </cfquery>
                <cfset result["cashout"] = data7>
                <cfcatch></cfcatch>
            </cftry>
        </cfif>

        <cfif listfindnocase(arguments.scopes,"raw_cash")>
            <cfquery dbtype="odbc" datasource="hola" name="data2">
                select *
                from api_cashtransaction t, api_openpaycardevent o
                where  (t.tracing_id = <cfqueryparam value="#tracing_id#">
                or t."TID" = '#tracing_id#')
                and t.tracing_id = o.tracing_id
                order by t.date_created asc
            </cfquery>
            <cfset result["raw_cash"] = data2>
        </cfif>


        <cfreturn result>
    </cffunction>

    <cffunction name="doLogin" hint="Authorize User">
        <cfargument name="userEmail">
        <cfargument name="userPass">

        <cfset result = structnew()>

        <cfquery dbtype="odbc" datasource="#application.datasource#" name="login">
            select top 1 * from _users where userEmail = <cfqueryparam value="#arguments.userEmail#">
            and userPass = <cfqueryparam value="#arguments.userPass#">
        </cfquery>

        <cfif login.recordcount eq 0>
            <cfset result["success"] = false>
            <cfset result["msg"] = "Datos Invalidos">
            <cfreturn result>
        </cfif>

        <cfif not(login.isActive)>
            <cfset result["success"] = false>
            <cfset result["msg"] = "Cuenta Bloqueada">
            <cfreturn result>
        </cfif>

        <cfset result["userid"] = login.userId>
        <cfset result["username"] = login.username>
        <cfset result["success"] = true>
        <cfreturn result>

    </cffunction>

    <cffunction name="getMerchants" hint="Get Merchant Catalog">
        <cfset MQ = querynew("MSID,merchant_name","varchar,varchar")>
        <cfset queryaddrow(MQ)>
        <cfset querysetcell(MQ,"MSID","0")>
        <cfset querysetcell(MQ,"merchant_name","TOTAL")>

        <cfquery dbtype="odbc" datasource="hc" name="all">
            select distinct MSID, merchant_name from TX_Core order by merchant_name
        </cfquery>

        <cfloop query="all">
            <cfset queryaddrow(MQ)>
            <cfset querysetcell(MQ,"MSID",MSID)>
            <cfset querysetcell(MQ,"merchant_name",merchant_name)>
        </cfloop>

        <cfreturn MQ>
    </cffunction>

    <cffunction name="getAcceptance" hint="Build Acceptance Dashboard">
        <cfargument name="onlyNet" default="true">
        <cfargument name="filterID" default="0" required="false">
        <cfargument name="qYear" default="#year(now())#">
        <cfargument name="qMonth" default="0" required="false">
        <cfargument name="approved_Declined" default="A" required="false">
        <cfargument name="is_for_local_merchant" default="0" required="false">
        
        <cfswitch expression="#arguments.filterID#">
            <cfcase value="0">
                <!--- Container Q --->
                <cfset AcceptQ = querynew("MSID,Merchant,myMonth,myYear,APPTXS,APPAMT,APPTKT,DECTXS,DECAMT,DECTKT,TOTTXS,TOTAMT,TOTTKT,ACCTXS,ACCAMT,isNetTx",
                "varchar,varchar,integer,integer,integer,decimal,decimal,integer,decimal,decimal,integer,decimal,decimal,decimal,decimal,bit")>

                <!--- ADMIN Header --->
                <cfloop from="3" to="#month(now())#" index="M">
                    <cfset queryaddrow(AcceptQ)>
                    <cfset querysetcell(AcceptQ,"MSID","0")>

                    <!--- How many merchants in period --->
                    <cfquery dbtype="odbc" datasource="hc" name="cuenta">
                        select count(distinct MSID) as total 
                        from TX_Core 
                        where month(date_created) = #M#
                        and year(date_created) = #qyear# 
                        <cfif arguments.onlyNet>
                            and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                            and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                        </cfif> 
                        and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                    </cfquery>

                    <cfset querysetcell(AcceptQ,"Merchant","TOTAL: #cuenta.total#")>
                    <cfset querysetcell(AcceptQ,"myMonth",M)>
                    <cfset querysetcell(AcceptQ,"myYear",arguments.qyear)>
                </cfloop>

                <!--- All Merchants --->
                <cfquery dbtype="odbc" datasource="hc" name="merchants">
                select distinct MSID, merchant_name from TX_Core 
                order by merchant_name
                </cfquery>

                <!--- Initial Merchant Entries Container --->
                <cfloop query="merchants">
                    <cfset thisID = MSID>
                    <cfloop from="3" to="#month(now())#" index="M">
                        <cfif thisID neq 'TOTAL'>
                            <cfset queryaddrow(AcceptQ)>
                            <cfset querysetcell(AcceptQ,"MSID",thisID)>
                            <cfset querysetcell(AcceptQ,"Merchant",merchant_name)>
                            <cfset querysetcell(AcceptQ,"MyMonth",M)>
                            <cfset querysetcell(AcceptQ,"myYear",arguments.qyear)>
                            <cfset querysetcell(AcceptQ,"isNetTx",booleanformat(arguments.onlyNet))>

                        </cfif>
                    </cfloop>
                </cfloop>

                <!--- Loop for All --->
                <cfloop query="AcceptQ">
                    <!--- Accepted --->
                    <cfquery dbtype="odbc" datasource="hc" name="Q1">
                        select count(*) as total, sum(amount) as monto, sum(amount)/count(*) as tkt
                        from TX_Core
                        where month(date_created) = <cfqueryparam value="#myMonth#">
                        and year(date_created) = <cfqueryparam value="#arguments.qyear#">
                        <cfif MSID neq '0'>
                            and MSID = <cfqueryparam value="#MSID#">
                        </cfif>
                        and approved_Declined = 'A'
                        <cfif arguments.onlyNet>
                            and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                            and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                        </cfif> 
                        and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                    </cfquery>
                
                    <!--- Declined --->
                    <cfquery dbtype="odbc" datasource="hc" name="Q2">
                    select count(*) as total, sum(amount) as monto, sum(amount)/count(*) as tkt
                    from TX_Core
                    where month(date_created) = <cfqueryparam value="#myMonth#">
                    and year(date_created) = <cfqueryparam value="#arguments.qyear#">
                    <cfif MSID neq '0'>
                        and MSID = <cfqueryparam value="#MSID#">
                    </cfif>
                    and approved_Declined = 'D'
                    <cfif arguments.onlyNet>
                        and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                        and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                    </cfif> 
                    and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">

                    </cfquery>

                    <!--- Prevent Division by Zero Errors --->
                    <cfif not(isnumeric(q1.total))><cfset q1.total = 0></cfif>
                    <cfif not(isnumeric(q1.monto))><cfset q1.monto = 0></cfif>
                    <cfif not(isnumeric(q1.tkt))><cfset q1.tkt = 0></cfif>
                    <cfif not(isnumeric(q2.total))><cfset q2.total = 0></cfif>
                    <cfif not(isnumeric(q2.monto))><cfset q2.monto = 0></cfif>
                    <cfif not(isnumeric(q2.tkt))><cfset q2.tkt = 0></cfif>

                    <!--- Insert Values --->
                    <cfset querysetcell(AcceptQ,"APPTXS",q1.total,AcceptQ.currentRow)>
                    <cfset querysetcell(AcceptQ,"APPAMT",q1.monto,AcceptQ.currentRow)>
                    <cfset querysetcell(AcceptQ,"APPTKT",q1.tkt,AcceptQ.currentRow)>
                    <cfset querysetcell(AcceptQ,"DECTXS",q2.total,AcceptQ.currentRow)>
                    <cfset querysetcell(AcceptQ,"DECAMT",q2.monto,AcceptQ.currentRow)>
                    <cfset querysetcell(AcceptQ,"DECTKT",q2.tkt,AcceptQ.currentRow)>
                    <cfset querysetcell(AcceptQ,"TOTTXS",q1.total+q2.total,AcceptQ.currentRow)>
                    <cfset querysetcell(AcceptQ,"TOTAMT",q1.monto+q2.monto,AcceptQ.currentRow)>
                    <cfif q1.total+q2.total gt 0>
                        <cfset querysetcell(AcceptQ,"TOTTKT",(q1.monto+q2.monto)/(q1.total+q2.total),AcceptQ.currentRow)>
                        <cfset querysetcell(AcceptQ,"ACCTXS",(q1.total/(q1.total+q2.total)*100),AcceptQ.currentRow)>
                    <cfelse>
                        <cfset querysetcell(AcceptQ,"TOTTKT",0,AcceptQ.currentRow)>
                        <cfset querysetcell(AcceptQ,"ACCTXS",0,AcceptQ.currentRow)>
                    </cfif>
                    <cfif q1.monto+q2.monto gt 0>
                        <cfset querysetcell(AcceptQ,"ACCAMT",(q1.monto/(q1.monto+q2.monto)*100),AcceptQ.currentRow)>
                    <cfelse>
                        <cfset querysetcell(AcceptQ,"ACCAMT",0,AcceptQ.currentRow)>
                    </cfif>

                </cfloop>
                <cfreturn AcceptQ>
            </cfcase>

            <cfdefaultcase>
                <cfquery dbtype="odbc" datasource="hc" name="AcceptQ">
                    select * from TX_core 
                    where msid = <cfqueryparam value="#arguments.filterID#">
                    <cfif arguments.onlyNet>
                        and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                        and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                    </cfif> 
                    and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                    and year(date_created) = <cfqueryparam value="#arguments.qyear#">
                    and month(date_created) = <cfqueryparam value="#arguments.qmonth#">
                    and approved_Declined = <cfqueryparam value="#arguments.approved_Declined#"> 
                    order by date_Created DESC
                 </cfquery>
                <cfreturn AcceptQ>
            </cfdefaultcase>
        </cfswitch>
    </cffunction>    

    <cffunction name="getDeclines" hint="Build Declines Dashboard">
        <cfargument name="onlyNet" default="true">
        <cfargument name="filterID" default="0" required="false">
        <cfargument name="qYear" default="#year(now())#">
        <cfargument name="qMonth" default="0" required="false">
        <cfargument name="PEC" default="0" required="false">
        <cfargument name="is_for_local_merchant" required="false" default="false">

        <cfswitch expression="#arguments.filterID#">
            <cfcase value="0">

                <cfset result = structnew()>

                <cfset DeclineQ1 = querynew("total,totalpct,monto,mymonth,myyear,cash_decline_code,processor_decline_code,processor_decline_message,processor_id,cdc,isNetTx",
                                        "integer,decimal,decimal,integer,integer,varchar,varchar,varchar,varchar,varchar,bit")>

                <!--- ADMIN Header --->
                <cfloop from="3" to="#month(now())#" index="M">
                    <cfquery dbtype="odbc" datasource="hc" name="Admin">
                        select count(*) as total, sum(amount) as monto
                        from TX_Core
                        where approved_Declined = 'D'
                        and month(date_created) = #M#
                        and year(date_created) = #QYear#
                        <cfif arguments.onlyNet>
                            and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                            and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                        </cfif> 
                        and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">

                    </cfquery>

                    <cfquery dbtype="odbc" datasource="hc" name="AdminB">
                        select count(*) as total, sum(amount) as monto
                        from TX_Core
                        where approved_Declined = 'D'
                        and month(date_created) = #M#
                        and year(date_created) = #QYear#
                        <cfif arguments.onlyNet>
                            and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                            and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                        </cfif> 
                        and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                    </cfquery>


                    <cfset queryaddRow(DeclineQ1)>
                    <cfset querysetCell(DeclineQ1,"total",admin.total)>
                    <cfset querysetCell(DeclineQ1,"monto",admin.monto)>
                    <cfset querysetCell(DeclineQ1,"cash_decline_code",0)>
                    <cfset querysetCell(DeclineQ1,"cdc",0)>
                    <cfset querysetCell(DeclineQ1,"processor_decline_code",0)>
                    <cfset querysetCell(DeclineQ1,"processor_decline_message","")>
                    <cfset querysetCell(DeclineQ1,"myMonth",M)>
                    <cfset querysetCell(DeclineQ1,"myYear",arguments.Qyear)>
                    <cfif adminB.total EQ 0>
                        <cfset querysetCell(DeclineQ1,"totalPct",0)>
                    <cfelse>
                        <cfset querysetCell(DeclineQ1,"totalPct",(admin.total/adminB.total)*100)>
                    </cfif>
                    <cfset querysetCell(DeclineQ1,"processor_id",0)>
                    <cfset querysetcell(DeclineQ1,"isNetTx",booleanformat(arguments.onlyNet))>
                </cfloop>

                
                <cfloop from="3" to="#month(now())#" index="M">
                    <cfset EsteMes = M>
        
                    <cfquery dbtype="odbc" datasource="hc" name="D1">
                        select count(*) as total, sum(amount) as monto, cash_decline_code, processor_decline_code, processor_decline_message, processor_id
                        from TX_Core
                        where approved_Declined = 'D'
                        and month(date_created) = #EsteMes#
                        and year(date_created) = #QYear#
                        <cfif arguments.onlyNet>
                            and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                            and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                        </cfif> 
                        and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                        group by cash_decline_code, processor_decline_code, processor_id, processor_decline_message
                        order by total desc
                    </cfquery>
        
                    <cfquery dbtype="odbc" datasource="hc" name="D1B">
                        select count(*) as total, sum(amount) as monto
                        from TX_Core
                        where approved_Declined = 'D'
                        and month(date_created) = #EsteMes#
                        and year(date_created) = #QYear#
                        <cfif arguments.onlyNet>
                            and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                            and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                        </cfif> 
                        and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                    </cfquery>
        
                    <cfloop query="D1">
        
                        <cfset cdc = Application.admin.getErrorCodeMeaning(cash_decline_code)>
        
                        <cfif len(trim(cdc)) gt 0>
                            <cfset queryaddRow(DeclineQ1)>
                            <cfset querysetCell(DeclineQ1,"total",total)>
                            <cfset querysetCell(DeclineQ1,"monto",monto)>
                            <cfset querysetCell(DeclineQ1,"cash_decline_code",cash_decline_code)>
                            <cfset querysetCell(DeclineQ1,"cdc",cdc)>
                            <cfset querysetCell(DeclineQ1,"processor_decline_code",processor_decline_code)>
                            <cfset querysetCell(DeclineQ1,"processor_decline_message",processor_decline_message)>
                            <cfset querysetCell(DeclineQ1,"myMonth",EsteMes)>
                            <cfset querysetCell(DeclineQ1,"myYear",arguments.Qyear)>
                            <cfset querysetCell(DeclineQ1,"totalPct",(total/D1B.total)*100)>
                            <cfset querysetCell(DeclineQ1,"processor_id",processor_id)>
                            <cfset querysetcell(DeclineQ1,"isNetTx",booleanformat(arguments.onlyNet))>
                        </cfif>
        
                    </cfloop>
        
                </cfloop>
        
                <cfquery dbtype="query" name="ordena">
                    select * from DeclineQ1 order by cash_decline_code, processor_decline_code, myMonth
                </cfquery>
        
                <cfset result["d1"] = ordena>
        
                <cfreturn ordena>

            </cfcase>
            <cfdefaultcase>
                <cfquery dbtype="odbc" datasource="hc" name="AcceptQ">
                    select * from TX_core 
                    where approved_Declined = <cfqueryparam value="D"> 
                    <cfif arguments.onlyNet>
                            and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                            and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                        </cfif> 
                        and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                    <cfif arguments.Pec neq 0>
                        and processor_decline_code = <cfqueryparam value="#arguments.PEC#">
                    </cfif>
                    and cash_decline_code = <cfqueryparam value="#arguments.filterID#">
                    and year(date_created) = <cfqueryparam value="#arguments.qyear#">
                    and month(date_created) = <cfqueryparam value="#arguments.qmonth#">
                    order by date_Created DESC
                 </cfquery>
                <cfreturn AcceptQ>
            </cfdefaultcase>

        </cfswitch>

    </cffunction>    

    <cffunction name="getErrorCodes" hint="gets unique Cash-errors from DB">
        <cfset MQ = querynew("cash_decline_code,cash_label","varchar,varchar")>

        <cfset queryaddrow(MQ)>
        <cfset querysetcell(MQ,"cash_decline_code",0)>
        <cfset querysetcell(MQ,"cash_label","total")>


        <cfquery dbtype="odbc" datasource="hc" name="all">
            select distinct cash_decline_code from TX_Core order by cash_decline_code
        </cfquery>

        <cfloop query="all">
            <cfset queryaddrow(MQ)>
            <cfset querysetcell(MQ,"cash_decline_code",cash_decline_code)>
            <cfset querysetcell(MQ,"cash_label",Application.admin.getErrorCodeMeaning(trim(cash_decline_code)))>
        </cfloop>

        <cfreturn MQ>
    </cffunction>

    <cffunction name="getErrorCodeMeaning" hint="Converts code to human text">
        <cfargument name="cash_decline_code">

        <cfset CDC = arguments.cash_decline_code>
        <cfloop from="1" to="#arraylen(this.setup.cashDeclineCodes)#" index="C">
            <cfif this.setup.cashDeclineCodes[C].code eq cash_decline_code>
                <cfset CDC = this.setup.cashDeclineCodes[C].value>
            </cfif>
        </cfloop>

        <cfreturn CDC>
    </cffunction>

    <cffunction name="getProcessorName" hint="returns Processor Name from ID">
        <cfargument name="processor_id">

        <cfswitch expression="#arguments.processor_id#">
            <cfcase value="1"><cfreturn "Openpay"></cfcase>
            <cfcase value="2"><cfreturn "Stripe"></cfcase>
            <cfcase value="4"><cfreturn "Banorte"></cfcase>
            <cfdefaultcase><cfreturn "N/A"></cfdefaultcase>
        </cfswitch>

    </cffunction>

    <cffunction name="getStats" hint="Build Processor Dashboard">
        <cfargument name="qYear" default="#year(now())#">
        <cfargument name="onlyNet" default="true">
        <cfargument name="is_for_local_merchant" required="false" default="false">
        
        <cfset stats = querynew("mytype,mymonth,myyear,myfilter1,myfilter2,myvalue1,myvalue2,mypct1,priority")>

        <!---
        <cfloop from="3" to="#month(now())#" index="M">
        --->
        <cfset M = 1> <!--- removed month --->
            <cfquery dbtype="odbc" datasource="hc" name="processors">
                select count(*) as total, sum(amount) as monto, processor_id, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                group by processor_id, approved_Declined 
                order by monto desc
            </cfquery>

            <cfloop query="processors">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","Processors")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",processor_id)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",1)>
            </cfloop>

            <cfquery dbtype="odbc" datasource="hc" name="threes">
                select count(*) as total, sum(amount) as monto, triggered_3ds, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                group by triggered_3ds, approved_Declined
                order by monto desc
            </cfquery>


            <cfloop query="threes">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","3ds")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",triggered_3ds)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",3)>
            </cfloop>


            <cfquery dbtype="odbc" datasource="hc" name="banks">
                select count(*) as total, sum(amount) as monto, bank_name, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                group by bank_name, approved_Declined 
                order by monto desc
            </cfquery>


            <cfloop query="banks">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","Issuers")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",bank_name)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",10)>
            </cfloop>


            <cfquery dbtype="odbc" datasource="hc" name="ecis">
                select count(*) as total, sum(amount) as monto, triggered_3ds, mapped_eci
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">

                group by triggered_3ds, mapped_eci 
                order by monto desc
            </cfquery>


            <cfloop query="ecis">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","Ecis")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",triggered_3ds)>
                <cfset querysetcell(stats,"myfilter2",mapped_eci)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",4)>
            </cfloop>            



            <cfquery dbtype="odbc" datasource="hc" name="cd">
                select count(*) as total, sum(amount) as monto, card_type, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                group by card_type, approved_Declined 
                order by monto desc
            </cfquery>


            <cfloop query="cd">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","cardtype")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",card_type)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",5)>

            </cfloop>            


            <cfquery dbtype="odbc" datasource="hc" name="cd">
                select count(*) as total, sum(amount) as monto, submission_channel, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">

                group by submission_channel, approved_Declined 
                order by monto desc
            </cfquery>


            <cfloop query="cd">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","submissionChannel")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",submission_channel)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",2)>

            </cfloop>            


            <cfquery dbtype="odbc" datasource="hc" name="cd">
                select count(*) as total, sum(amount) as monto, fraud_success, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                group by fraud_success, approved_Declined 
                order by monto desc
            </cfquery>


            <cfloop query="cd">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","fraudSuccess")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",fraud_success)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",7)>
            </cfloop>            


            <cfquery dbtype="odbc" datasource="hc" name="cd">
                select count(*) as total, sum(amount) as monto, isMSI, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                group by isMSI, approved_Declined 
                order by monto desc
            </cfquery>


            <cfloop query="cd">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","isMSI")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",isMSI)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",8)>
            </cfloop>  


            <cfquery dbtype="odbc" datasource="hc" name="cd">
                select count(*) as total, sum(amount) as monto, hasToken, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">

                group by hasToken, approved_Declined 
                order by monto desc
            </cfquery>


            <cfloop query="cd">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","hasToken")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",hasToken)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",9)>
            </cfloop>  


            <cfquery dbtype="odbc" datasource="hc" name="cd">
                select count(*) as total, sum(amount) as monto, is_for_local_merchant, approved_Declined
                from TX_core 
                where year(date_created) = <cfqueryparam value="#arguments.qyear#" >
                <cfif arguments.onlyNet>
                    and cash_decline_code NOT IN (<cfqueryparam value="#get_nonNetCashErrorCodes('list')#" list="true">)
                    and MSID NOT IN (<cfqueryparam value="#get_testMerchants('list')#" list="true">)
                </cfif> 
                and is_for_local_merchant = <cfqueryparam value="#arguments.is_for_local_merchant#">
                group by is_for_local_merchant, approved_Declined 
                order by monto desc
            </cfquery>


            <cfloop query="cd">
                <cfset queryaddrow(stats)>
                <cfset querysetcell(stats,"mytype","isForLocalMerchant")>
                <cfset querysetcell(stats,"mymonth",M)>
                <cfset querysetcell(stats,"myyear",arguments.qyear)>
                <cfset querysetcell(stats,"myfilter1",is_for_local_merchant)>
                <cfset querysetcell(stats,"myfilter2",approved_Declined)>
                <cfset querysetcell(stats,"myvalue1",total)>
                <cfset querysetcell(stats,"myvalue2",monto)>
                <cfset querysetcell(stats,"priority",6)>
            </cfloop>  


        <!---
        </cfloop>
        --->

        <cfquery dbtype="query" name="unicos">
            select distinct myType from stats
        </cfquery>
        <cfloop query="#unicos#">
            <cfquery dbtype="query" name="base">
                select sum(myvalue1) as total from stats 
                where myType = <cfqueryparam value="#myType#">
            </cfquery>
            <cfloop query="stats">
                <cfif myType eq '#myType#'>
                    <cfset pct = (myvalue1/base.total)*100>
                    <cfset querysetcell(stats,"mypct1",pct,stats.currentRow)>
                </cfif>
            </cfloop>
        </cfloop>



        <cfquery dbtype="query" name="ordena">
            select * from stats order by priority, mymonth, myType, myfilter1, myfilter2
        </cfquery>

        <cfreturn ordena>

    </cffunction>    

    <cffunction name="getSettings" hint="returns initialized JSON">
        <cfreturn this.setup>
    </cffunction>

    <cffunction name="saveSettings" hint="updates JSON">
        <cfargument name="update" hint="cashDeclineCodes,testMerchants">
        <cfargument name="data" hint="cash_label,cash_decline_code"> 

        <cfswitch expression="#arguments.update#">
            <cfcase value="cashDeclineCodes">

                <cfset myJSON = Application.admin.getSettings()>
                <cfset myCodes = myJSON.cashDeclineCodes>
                <cfset foundItem = false>

                <!--- Update Description if found --->
                <cfloop from="1" to="#arraylen(myCodes)#" index="MC">
                    <cfif myCodes[MC].code eq arguments.data.cash_decline_code>
                        <cfset foundItem = true>
                        <cfset myCodes[MC].value = arguments.data.cash_label>
                    </cfif>
                </cfloop>

                <!--- if new code then add --->
                <cfif not(foundItem)>
                    <cfset tempItem = structnew()>
                    <cfset tempItem["code"] = arguments.data.cash_decline_code>
                    <cfset tempitem["value"] =  arguments.data.cash_label>
                    <cfset arrayappend(myCodes,tempItem)>
                </cfif>

                <!--- Now Check Array --->
                <cfset myNonNet = myJSON.nonNetCashErrorCodes>
                <cfset findPos = arrayfindnocase(myNonNet,trim(arguments.data.cash_decline_code))>
                <cfif not(isdefined("arguments.data.CASH_SWITCH"))>
                    <!--- delete if exists --->
                    <cfif findPos gt 0><cfset arraydeleteat(myNonNet,findPos)></cfif>
                <cfelse>
                    <!--- insert if not exits --->
                    <cfif findPos eq 0><cfset arrayappend(myNonNet,trim(arguments.data.cash_decline_code))></cfif>
                </cfif>

                <!--- Save JSON --->
                <cfset NewCodes = myJSON>
                <cfset NewCodes["cashDeclineCodes"] = myCodes>
                <cfset NewCodes["nonNetCashErrorCodes"] = myNonNet>
                <cffile action="write" file="#expandpath('/admin/setup.json')#" output="#serializeJSON(NewCodes)#" >

                <cfreturn true>
            </cfcase>

        </cfswitch>

    </cffunction>


    <cffunction name="hasPermit" hint="for Users in Web">
        <cfargument name="userId">
        <cfargument name="assetName">

        <cfquery dbtype="odbc" datasource="#Application.datasource#" name="P" maxrows="1">
            select top 1 * from _permits 
            where userid = <cfqueryparam value="#arguments.userID#">
            and assetName = <cfqueryparam value="#arguments.assetName#">
        </cfquery>

        <cfif P.recordcount eq 1>
            <cfreturn true>
        <cfelse>
            <cfreturn false>
        </cfif>

    </cffunction>

    <cffunction name="Search" hint="Searches">
        <cfargument name="q">

        <cfquery dbtype="odbc" datasource="#Application.datasource#" name="search">
        select * from TX_Core 
        where tracing_id LIKE <cfqueryparam value="%#arguments.q#%">
            or MSID Like <cfqueryparam value="%#arguments.q#%">
            <cfif isnumeric(arguments.q)>
                or amount = <cfqueryparam value="#arguments.q#">
            </cfif>
            or merchant_name like <cfqueryparam value="%#arguments.q#%">
            or cash_transaction_email like <cfqueryparam value="%#arguments.q#%">
            or bank_name like  <cfqueryparam value="%#arguments.q#%">
            or card_number like <cfqueryparam value="%#arguments.q#%">
        order by date_created desc
        </cfquery>

        <cfreturn search>
    </cffunction>

    <cffunction name="isNetTX" hint="returns true of false if TX is net vs gross">
        <cfargument name="tracing_id">

        <cfquery dbtype="odbc" datasource="#application.datasource#" name="checa">
            select cash_decline_code, MSID from TX_CORE 
            where tracing_id = <cfqueryparam value="#arguments.tracing_id#">
        </cfquery>

        <cfset nonNetCashErrorCodes = application.admin.get_nonNetCashErrorCodes('list')>
        <cfset testMerchants = application.admin.get_testMerchants('list')>

        <cfif listfindnocase(nonNetCashErrorCodes,trim(checa.cash_decline_code)) 
            or not(listfindnocase(testMerchants,trim(checa.MSID)))>
            <cfreturn false>
        <cfelse> 
            <cfreturn true>
        </cfif>
 
    </cffunction>

    <cffunction name="newUser" hint="creates a new User">
        <cfargument name="userName">
        <cfargument name="userEmail">
        <cfargument name="userPass">

        <cfquery dbtype="odbc" datasource="#application.datasource#" name="checa">
            select userID from _users where userEmail = <cfqueryparam value="#arguments.userEmail#">
        </cfquery>

        <cfif checa.recordcount neq 0>
            <cfreturn 0>
        </cfif>

        <cfquery dbtype="odbc" datasource="#application.datasource#" result="inserta">
            insert into _users (userName,userEmail,userPass)
            values
            (
                <cfqueryparam value="#arguments.userName#">
                ,<cfqueryparam value="#arguments.userEmail#">
                ,<cfqueryparam value="#arguments.userPass#">
            )
        </cfquery>

        <cfreturn inserta["GENERATEDKEY"]>

    </cffunction>

    <cffunction name="getUsers" hint="gets all Users">
        <cfquery dbtype="odbc" datasource="#application.datasource#" name="all">
            select *, '' as mypermits from _users 
            order by userName asc
        </cfquery>

        <cfloop query="all">
            <cfquery dbtype="odbc" datasource="#application.datasource#" name="X">
            select distinct assetName from _permits where userid = <cfqueryparam value="#userID#">
            </cfquery>    
            <cfif X.recordcount gt 0>
                <cfset all.mypermits[all.currentRow] = valuelist(X.assetName)>
            <cfelse>
                <cfset all.mypermits[all.currentRow] = "">

            </cfif>
        </cfloop>

        <cfreturn all>
    </cffunction>

    <cffunction name="updateUser" hint="Updates Users">
        <cfargument name="data" hint="form">

        <cfquery dbtype="odbc" datasource="#Application.datasource#" name="actualiza">
            Update _users set userName = userName 
                <cfif isdefined("arguments.data.isActive")>,isActive = 1<cfelse>,isActive = 0</cfif>
                <cfif len(trim(arguments.data.newPass)) gt 0>,userPass = <cfqueryparam value="#arguments.data.newPass#"></cfif>
            where userid = <cfqueryparam value="#arguments.data.userid#">        
        </cfquery>

        <cfquery dbtype="odbc" datasource="#application.datasource#" name="resetPermit">
        delete from _permits where userid = <cfqueryparam value="#arguments.data.userID#">
        </cfquery>

        <cfloop list="#arguments.data.Permits#" index="P">
            <cfquery dbtype="odbc" datasource="#application.datasource#" name="insertPermit">
                insert into _permits (assetName,userID) 
                values (<cfqueryparam value="#P#">,<cfqueryparam value="#arguments.data.userid#">)
            </cfquery>
        </cfloop>

        <cfreturn true>
    </cffunction>
</cfcomponent>