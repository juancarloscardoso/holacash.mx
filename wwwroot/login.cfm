
<cfif isdefined("form.userEmail") and isdefined("form.userPass")>
	<cfset isLoggedIn = Application.admin.doLogin(form.userEmail,form.userPass)>
    <cfif isLoggedIn.success>
		<cfset session.userid = isLoggedIn.userid>
		<cfset session.userName = isLoggedin.userName>
		<cflocation url="index.cfm" addtoken="false">
    <cfelse>
        <h1><cfoutput>#isLoggedIn.MSG#</cfoutput></h1>
    </cfif>
</cfif>


<!DOCTYPE html>
<html lang="en">
<cfmodule template="/includes/head.cfm">
<body class='pace-top'>
	<!-- BEGIN #app -->
	<div id="app" class="app app-full-height app-without-header">
		<!-- BEGIN login -->
		<div class="login">
			<!-- BEGIN login-content -->
			<div class="login-content">
				<form action="login.cfm" method="POST" name="login_form">
					<h1 class="text-center">HOLA*OPS</h1>
					<div class="text-white text-opacity-50 text-center mb-4">
						Por favor identificate para continuar.
					</div>
					<div class="mb-3">
						<label class="form-label">Email <span class="text-danger">*</span></label>
						<input type="email" name="userEmail" class="form-control form-control-lg bg-white bg-opacity-5" value="" placeholder="" required />
					</div>
					<div class="mb-3">
						<div class="d-flex">
							<div class="d-flex">
								<label class="form-label">Contrase&ntilde;a <span class="text-danger">*</span></label>
							</div>
						</div>
						<input type="password" name="userPass" class="form-control form-control-lg bg-white bg-opacity-5" value="" placeholder="" required />
					</div>
					<div class="mb-3">
						<div class="form-check">
							<!--- <input class="form-check-input" type="checkbox" value="" id="customCheck1" /> --->
							<label class="form-check-label" for="customCheck1"><!--- remeber me ---></label>
						</div>
					</div>                    

					<button type="submit" class="btn btn-outline-theme btn-lg d-block w-100 fw-500 mb-3">Ingresar</button>
				</form>
			</div>
			<!-- END login-content -->
		</div>
		<!-- END login -->
		
		<!-- BEGIN btn-scroll-top -->
		<a href="#" data-toggle="scroll-to-top" class="btn-scroll-top fade"><i class="fa fa-arrow-up"></i></a>
		<!-- END btn-scroll-top -->
        <!---
        <cfinclude template="/reps/includes/themepanel.cfm">
        --->
	</div>
	<!-- END #app -->

	<cfmodule template="/includes/footer.cfm" page="Login">
    
</body>
</html>