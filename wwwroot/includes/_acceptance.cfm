<!--- PERMIT --->
<cfset Permit = "Dashboards.acceptance"><cfset hasPermit = Application.admin.hasPermit(session.userID,Permit)>
<cfif Not(hasPermit)><script>alert("Permission Denied");location.href="index.cfm";</script><cfabort></cfif>

<!-- BEGIN #content -->
<div id="content" class="app-content">
	<!-- BEGIN row -->
    <div class="row">
        <div class="col-12">
            <cfset Acceptance = Application.admin.getAcceptance(session.onlyNet,0,year(now()),0,"A",session.isForLocal)>
            <cfset Merchants = Application.admin.getMerchants()>

            <cfparam name="qYear" default="#year(now())#">

            <div class="float-end">
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" name="isforlocal" id="isforLocal" <cfif session.isForLocal> checked </cfif>>
                    <label class="form-check-label" for="isforLocal">Is For Local Merchant</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" name="onlyNet" id="onlyNet" <cfif session.onlyNet> checked </cfif>>
                    <label class="form-check-label" for="onlyNet">Only Net Transactions</label>
                </div>
    
            </div>


            <h1>Acceptance Summary <cfoutput>#Qyear#</cfoutput></h1>


            <cfoutput>

                    <cfloop query="#Merchants#">
                        <cfquery dbtype="query" name="data">
                            select * from acceptance where MSID = <cfqueryparam value="#MSID#">
                            <cfif session.onlyNet and msid gt 0> and isNetTx = 1</cfif>
                        </cfquery>
                        <cfquery dbtype="query" name="suma">
                            select Sum(totamt) as total from acceptance where MSID = <cfqueryparam value="#MSID#">
                            <cfif session.onlyNet and msid gt 0> and isNetTx = 1</cfif>
                        </cfquery>

                        <cfif (data.recordcount gt 0 and suma.total gt 0) or MSID eq 0>
                        <table class="table" style="width: 100%;">
                            <thead>
                            <tr><th rowspan="2">Merchant</th><th rowspan="2">Month</th><th colspan="3">Approved</th><th colspan="3">Declined</th><th colspan="3">Total</th><th colspan="2">Acceptance</th></tr>
                            <tr><th>Txs</th><th>Amount</th><th>Ticket</th><th>Txs</th><th>Amount</th><th>Ticket</th><th>Txs</th><th>Amount</th><th>Ticket</th><th>Txs</th><th>Amount</th></tr>
                            </thead>

                            <cfset tempSum = structnew()>
                            <cfset tempSum["apptxs"] = 0>
                            <cfset tempSum["appamt"] = 0>
                            <cfset tempSum["apptkt"] = arraynew(1)>
                            <cfset tempSum["dectxs"] = 0>
                            <cfset tempSum["decamt"] = 0>
                            <cfset tempSum["dectkt"] = arraynew(1)>
                            <cfset tempSum["tottxs"] = 0>
                            <cfset tempSum["totamt"] = 0>
                            <cfset tempSum["tottkt"] = arraynew(1)>
                            <cfset tempSum["acctxs"] = arraynew(1)>
                            <cfset tempSum["accamt"] = arraynew(1)>

                            <cfloop query="data">
                                <cfif tottxs gt 0>
                                    <cfset tempSum["apptxs"] = tempSum.apptxs + apptxs>
                                    <cfset tempSum["appamt"] = tempSum.appamt + appamt>
                                    <cfset arrayappend(tempSum.apptkt,apptkt)>
                                    <cfset tempSum["dectxs"] = tempSum.dectxs + dectxs>
                                    <cfset tempSum["decamt"] = tempSum.decamt + decamt>
                                    <cfset arrayappend(tempSum.dectkt,dectkt)>
                                    <cfset tempSum["tottxs"] = tempSum.tottxs + tottxs>
                                    <cfset tempSum["totamt"] = tempSum.totamt + totamt>
                                    <cfset arrayappend(tempSum.tottkt,tottkt)>
                                    <cfset arrayappend(tempSum.acctxs,acctxs)>
                                    <cfset arrayappend(tempSum.accamt,accamt)>
                    
                                
                                        <tbody>
                                            <tr>
                                                <td class="width-77">#Merchant#</td>
                                                <td class="width-77">#monthasString(MyMonth)#</td>
                                                <cfif MSID neq 0 and apptxs gt 0>
                                                    <td class="left-border"><a class="btn btn-sm btn-primary" href="index.cfm?action=detail&filterId=#MSID#&type=Acceptance&onlyNet=#session.onlyNet#&qMonth=#MyMonth#&qyear=#myYear#&approveDecline=A"><b>#numberformat(APPTXS,'0,')#</b></a></td>
                                                <cfelse>
                                                    <td class="left-border">#numberformat(APPTXS,'0,')#</td>
                                                </cfif>
                                                <td class="width-77">$#numberformat(APPAMT,'0,')#</td>
                                                <td class="width-77">$#numberformat(APPTKT,'0,')#</th>
                                                <cfif MSID neq 0 and dectxs gt 0>
                                                    <td class="left-border"><a class="btn btn-sm btn-primary" href="index.cfm?action=detail&filterId=#MSID#&type=Acceptance&onlyNet=#session.onlyNet#&qMonth=#MyMonth#&qyear=#myYear#&approveDecline=D"><b>#numberformat(DECTXS,'0,')#</b></a></td>
                                                <cfelse>
                                                    <td class="left-border">#numberformat(DECTXS,'0,')#</td>
                                                </cfif>
                                                <td class="width-77">$#numberformat(DECAMT,'0,')#</td>
                                                <td class="width-77">$#numberformat(DECTKT,'0,')#</th>
                                                <td class="left-border">#numberformat(TOTTXS,'0,')#</td>
                                                <td class="width-77">$#numberformat(TOTAMT,'0,')#</td>
                                                <td class="width-77">$#numberformat(TOTTKT,'0,')#</td>
                                                <td class="left-border">#numberformat(ACCTXS,'0.00')#%</td>
                                                <td class="width-77">#numberformat(ACCAMT,'0.00')#%</td>
                                            </tr>
                                        </tbody> 
                                </cfif>                   

                        </cfloop>
                        <tfoot>
                            <tr>
                                <td colspan=2>YTD</td>
                                <td class="left-border">#numberformat(tempsum.APPTXS,'0,')#</td>
                                <td class="width-77">$#numberformat(tempsum.APPAMT,'0,')#</td>
                                <td class="width-77">$#numberformat(arrayavg(tempsum.APPTKT),'0,')#</th>
                                <td class="left-border">#numberformat(tempsum.DECTXS,'0,')#</td>
                                <td class="width-77">$#numberformat(tempsum.DECAMT,'0,')#</td>
                                <td class="width-77">$#numberformat(arrayavg(tempsum.DECTKT),'0,')#</th>
                                <td class="left-border">#numberformat(tempsum.TOTTXS,'0,')#</td>
                                <td class="width-77">$#numberformat(tempsum.TOTAMT,'0,')#</td>
                                <td class="width-77">$#numberformat(arrayavg(tempsum.TOTTKT),'0,')#</td>
                                <td class="left-border">#numberformat(arrayavg(tempsum.ACCTXS),'0.00')#%</td>
                                <td class="width-77">#numberformat(arrayavg(tempsum.ACCAMT),'0.00')#%</td>
                            </tr>


                            </tfoot>


                        </table> 
                        </cfif>

                    </cfloop>  

            </cfoutput>
        </div>
    </div>
</div>
