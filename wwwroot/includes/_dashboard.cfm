<cfset NNEC = Application.admin.get_nonNetCashErrorCodes('array')>
<cfset TMCH = Application.admin.get_testMerchants('array')>

<!-- BEGIN #content -->
<div id="content" class="app-content">


<cfif isdefined("url.tracing_id")>
    <cfset Ready = Application.admin.isDataReady(url.tracing_id)>
    <cfif Not(Ready)>
        <h3>Data Incompleta</h3>
        <h5>Unable to get full details. Data for this transaction is still synching.</h5>
        <h5>Please wait a few minutes and try again.</h5>
    <cfelse>

        <!-- BEGIN row -->
        <div class="row">
            <div class="col-12">

                <cfset TX = Application.admin.getTX(url.tracing_id)>

                <cfoutput>

                <h3>Transaction Snapshot</h3>

                <table class="styled-table table">
                    <tr>
                        <th>Created/Updated</th>
                        <td>#dateformat(tx.core.date_created,'dd/mmm/yyyy')# #timeformat(tx.core.date_created,'HH:MM')#</td>
                        <td>#dateformat(tx.core.importDate,'dd/mmm/yyyy')# #timeformat(tx.core.importDate,'HH:MM')#</td>
                    </tr>
                    <tr>
                        <th>Tracing_id</th>
                        <td>#url.tracing_id#</td>
                        <td>#Application.admin.getProcessorName(tx.core.processor_id)#</td>
                    </tr>
                    <tr>
                        <th>Transaction_id</th>
                        <td>#tx.core.original_cash_transaction_id#</td>
                        <td>#tx.core.submission_channel#</td>
                    </tr>
                    <tr>
                        <th>Merchant</th>
                        <td>#tx.core.MSID#</td>
                        <td>#tx.core.merchant_name#</td>
                    </tr>
                    <tr>
                        <th>Identity_User</th>
                        <td>#tx.core.identity_user_id#</td>
                        <td>#tx.core.cash_transaction_email#</td>
                    </tr>
                    <tr>
                        <th>Amount</th>
                        <td>$#numberformat(tx.core.amount,'0,.00')# MXP</td>
                        <td>#tx.core.description#</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td><cfif tx.core.approved_declined eq 'A'><span class="text-success">Approved</span><cfelse><span class="text-danger">Declined</span></cfif></td>
                        <td>
                            #Application.admin.getProcessorName(tx.core.processor_id)#                             
                        </td>
                    </tr>
                    <tr>
                        <th>Decline Code</th>
                        <td><cfif len(trim(tx.core.processor_decline_message)) eq 0>(null)<cfelse>#tx.core.processor_decline_message#</cfif></td>
                        <td><cfif len(trim(tx.core.cash_decline_code)) eq 0>(null)<cfelse>#tx.core.cash_decline_code#</cfif></td>
                    </tr>

                    <tr>
                        <th>Bin</th>
                        <td>#tx.core.card_number# (#trim(tx.core.card_type)#)</td>
                        <td>#tx.core.bank_name#</td>
                    </tr>
                    <tr>
                        <th>Flags</th>
                        <td colspan="2">
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="customSwitch1" disabled <cfif not(arrayfind(NNEC,tx.core.cash_decline_code))> checked </cfif> >
                                <label class="form-check-label" for="customSwitch1">Is Net Transaction</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="customSwitch2" disabled <cfif not(arrayfind(TMCH,tx.core.MSID))> checked </cfif> >
                                <label class="form-check-label" for="customSwitch2">Is Not Test Transaction</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="customSwitch3" disabled <cfif isboolean(tx.core.isMSI) and tx.core.isMSI> checked </cfif> >
                                <label class="form-check-label" for="customSwitch3">Is "MSI" Meses sin Interes</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="customSwitch4" disabled <cfif isboolean(tx.core.hasRefund) and not(tx.core.hasRefund)> checked </cfif> >
                                <label class="form-check-label" for="customSwitch4">Is not Refunded</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="customSwitch7" disabled <cfif isboolean(tx.core.is_for_local_merchant) and tx.core.is_for_local_merchant> checked </cfif> >
                                <label class="form-check-label" for="customSwitch7">Is for Local Merchant</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="customSwitch8" disabled <cfif isboolean(tx.core.fraud_success) and tx.core.fraud_success> checked </cfif> >
                                <label class="form-check-label" for="customSwitch8">Passed Fraud check</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="customSwitch5" disabled <cfif isboolean(tx.core.hasToken) and tx.core.hasToken> checked </cfif> >
                                <label class="form-check-label" for="customSwitch5">Has Tokenized Card</label>
                            </div>
                            <div class="form-check form-switch">
                                <input type="checkbox" class="form-check-input" id="customSwitch6" disabled <cfif isboolean(tx.core.triggered_3DS) and tx.core.triggered_3ds> checked </cfif> >
                                <label class="form-check-label" for="customSwitch6">Triggered 3DS <cfif len(tx.core.mapped_eci) gt 0 and isboolean(tx.core.triggered_3ds) and  tx.core.triggered_3ds><cfif (tx.core.mapped_eci neq -1 and tx.core.mapped_eci neq 5) ><span class="text-danger">(ECI: #Tx.core.mapped_eci#)</span><cfelse><span class="text-success">(ECI: #tx.core.mapped_eci#)</span></cfif></cfif></label>
                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <hr size="1">
                <h3>Transaction Log</h3>

                <table class="styled-table table">
                    <thead>
                        <tr><th>Date_Created</th><th>Processor_id</th><th>Processor_event_type</th><th>Response_status</th><th>Transaction_status</th><cfif tx.core.approved_declined eq 'D'><th>decline_code</th></cfif></tr>
                    </thead>
                    <cfloop query="TX.hola"> 
                        <tbody>
                        <tr>
                            <td>#dateformat(date_Created,'dd mmm')# #timeformat(date_created,'HH:MM')#</td>
                            <td>
                                #Application.admin.getProcessorName(tx.core.processor_id)#                             </td>
                            <td>#processor_event_type#</td>
                            <td>#response_status#</td>
                            <td>#transaction_status#</td>
                            <cfif tx.core.approved_declined eq 'D'><td>#cash_decline_code#<br>#processor_decline_code#</td></cfif>
                        </tr>
                        </tbody>
                    </cfloop>
                </table>
            </div>
        </div>

        <cfif isdefined("tx.status")>
        <div clas="row">
            <div class="col-12">

                <h3>Transaction Status</h3>
                <table class="styled-table table">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Captured</th>
                            <th>Completed</th>
                            <th>Settled</th>
                            <th>Refunded</th>
                            <th>Disbursed</th>
                            <th>Chargeback</th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfloop query="TX.status">
                            <tr>
                                <td>#transaction_type#</td>
                                <td>#captured#</td>
                                <td>#completed#</td>
                                <td>#settled#</td>
                                <td>#refunded#</td>
                                <td>#disbursed#</td>
                                <td>#chargeback#</td>
                            </tr>
                        </cfloop>
                    </tbody>

                </table>
            </div>
        </div>
        </cfif>        

        <div class="row">
            <div class="col-12">
                <table class="styled-table table"> 
                    <tr>
                        <th>Fraud Rule Context</th>
                        <th>Original Request</th>
                    </tr>
                    <tr>
                        <td>
                            <textarea class="form-control" cols="10" rows="20" disabled id="prettyJSONFormat1">#TX.fraud#</textarea>
                        </td>
                        <td>
                            <textarea class="form-control" cols="10" rows="20" disabled id="prettyJSONFormat2"><cfif isdefined("tx.request")>#TX.request#</cfif></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <cfif isdefined("TX.cashout") and tx.cashout.recordcount gt 0>
        <h3>Transaction Cash-out</h3>
        <div class="row">
            <div class="col-12">
                <table class="styled-table table">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Speed</th>
                        <th>Amount</th>
                        <th>Fee</th>
                        <th>Clabe</th>
                        <th>Target</th>
                        <th>STPID</th>
                    </tr>
                    </thead>                   
                    <tbody>
                        <cfloop query="tx.cashout">
                            <tr>
                                <td>#dateformat(Date,'dd/mmm')#</td>
                                <td>#cashout_speed#</td>
                                <td>#numberformat(Amount_plus_fee-Fee,'0,.0')#</td>
                                <td>#numberformat(Fee,'0,.0')#</td>
                                <td>#target_bank_clabe#</td>
                                <td>#target_name#</td>
                                <td>#stp_id#</td>
                            </tr>
                        </cfloop>
                    </tbody> 
                </table>

            </div>
        </div>
        </cfif>        


        <div class="row">
            <div class="col-12">

                <h3>Transaction Tracing</h3>
                <ol>
                <cfset thisMsg = "">
                <cfloop from="1" to="#arraylen(TX.trace.traces)#" index="T">
                    <cfset thisTrace = TX.trace.traces[T].msg>
                    <cfif thisTrace neq thisMsg>
                        <li>#thisTrace#</li>
                        <cfset thisMsg = thisTrace>
                    </cfif>
                </cfloop>
                </ol>

                </cfoutput>
            </div>
        </div>
    </cfif>

<cfelse>
 
        <cfset ALL = Application.admin.getAll(session.onlyNet,'DESC',1000,session.isForLocal)>



        <!-- BEGIN row -->
    <div class="row">
        <div class="col-12">
            <div class="float-end">
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" name="isforlocal" id="isforLocal" <cfif session.isForLocal> checked </cfif>>
                    <label class="form-check-label" for="isforLocal">Is For Local Merchant</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" name="onlyNet" id="onlyNet" <cfif session.onlyNet> checked </cfif>>
                    <label class="form-check-label" for="onlyNet">Only Net Transactions</label>
                </div>
    
            </div>
    

            <table class="table cell-border compact stripe table-responsive" id="dataTable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Tracing</th>
                        <th>Merchant</th>
                        <th>Amount</th>
                        <th>Bin</th>
                        <th>Status</th>
                        <th>Response</th>
                    </tr>

                </thead>
                <tbody>

                    <cfoutput query="ALL">
                        <cfset Ready = Application.admin.isDataReady(tracing_id)>

                        <tr>
                            <td>#dateformat(Date_created,'mm-dd')# #timeformat(Date_created,'HH:MM')#</td>
                                <td><a href="index.cfm?action=dashboard&tracing_id=#tracing_id#" class="btn btn-outline-theme me-2" <cfif not(ready)> disabled</cfif>>Detail <cfif not(ready)>(...)</cfif></a></td>
                            <td>#merchant_name#</td>
                            <td>#numberformat(amount,'0,.0')#</td>
                            <td>#card_number#</td>
                            <td><cfif Approved_Declined eq 'A'><span class="text-success">Approved</span><cfelse><span class="text-danger">Declined</span></cfif></td>
                            <td><cfif cash_decline_code eq Application.admin.getErrorCodeMeaning(cash_decline_code)>#cash_decline_code#<cfelse>#cash_decline_code# - #Application.admin.getErrorCodeMeaning(cash_decline_code)#</cfif>
                                    <cfif not(listfindnocase('DECLINE,APPROVAL',response_status))>- #response_status#</cfif>
                                    <cfif len(trim(processor_decline_message)) gt 0><br>-#processor_decline_message#</cfif>
                            </td>
                        </tr>
                    </cfoutput>


                </tbody>

            </table>


        </div>
    </div>

	<!-- END row -->
</cfif>

</div>
<!-- END #content -->

<script>
       var badJSON = document.getElementById('prettyJSONFormat1').value;
       var parseJSON = JSON.parse(badJSON);
       var JSONInPrettyFormat = JSON.stringify(parseJSON, undefined, 4);
       document.getElementById('prettyJSONFormat1').value = JSONInPrettyFormat;

       var badJSON = document.getElementById('prettyJSONFormat2').value;
       var parseJSON = JSON.parse(badJSON);
       var JSONInPrettyFormat = JSON.stringify(parseJSON, undefined, 4);
       document.getElementById('prettyJSONFormat2').value = JSONInPrettyFormat;

</script>