    <!--- PERMIT --->
    <cfset Permit = "Settings.Users"><cfset hasPermit = Application.admin.hasPermit(session.userID,Permit)>
    <cfif Not(hasPermit)><script>alert("Permission Denied");location.href="index.cfm";</script><cfabort></cfif>


    <cfif isdefined("form.userEmail")>
        <cfset newUsr = Application.admin.newUser(form.userName,form.userEmail,form.userPass)>
        <cfif newUsr eq 0>
            <script>alert("Error.. the account already exists");</script>
        </cfif>
    </cfif>


    <!-- BEGIN #content -->
    <div id="content" class="app-content">

        <cfif isdefined("form.userid")>
            <cfset Application.admin.updateUser(form)>
            <div class="alert alert-success alert-dismissable fade show p-3">
                <h5 class="alert-heading">Update successful</h5>
            </div>
        </cfif>

        <cfset All = Application.admin.getUsers()>


        <div class="row">
            <div class="col-6">
            <h1>Add New User</h1>

                <form name="newUser" method="post" action="index.cfm?action=users">
                <table class="table table-styled">
                    <tr>
                        <th>User Name</th>
                        <td><input type="text" class="form-control" required name="userName"></td>
                    </tr>
                    <tr>
                        <th>User Email</th>
                        <td><input type="text" class="form-control" required name="userEmail"></td>
                    </tr>
                    <tr>
                        <th>User Password</th>
                        <td><input type="text" class="form-control" required name="userPass"></td>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <th class="text-right"><input type="submit" class="btn btn-primary form-control" value="Register"></th>
                    </tr>
                </table>
                </form>                

            </div>        
        </div>
        
        <hr size="1">

        <div class="row">
            <div class="col-12">
                <table class="table table-styled">
                    <thead>
                        <tr>
                            <th>userID</th>             
                            <th>userName</th>             
                            <th>userEmail</th>   
                            <th>Status</th>             
                            <th>Permits (Cmd+C)</th>             
                            <th>userPass</th>          
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                        <cfloop query="All">
                            
                            <form name="resetPass" method="post" action="index.cfm?action=users">
                                <input type="hidden" name="userID" value="#userID#">

                            <tr>
                                <td>#userID#</td>
                                <td>#userName#</td>
                                <td>#userEmail#</td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input type="checkbox" class="form-check-input" name="isActive" id="customSwitch1" <cfif isActive> checked </cfif> >
                                        <label class="form-check-label" for="isActive"><cfif isActive>Active<cfelse>Blocked</cfif></label>
                                    </div>
                                </td>
                                <td>
                                    <cfset allPermits = Application.admin.getSettings().permits>
                                    <select name="Permits" multiple size="#arraylen(allPermits)#">
                                        <cfloop from="1" to="#arraylen(allPermits)#" index="P">
                                            <option <cfif listfindnocase(myPermits,allPermits[P])> selected </cfif>>#allPermits[P]#</option>
                                        </cfloop>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" name="newPass" class="form-control" placeholder="new password">
                                    <input type="submit" value="Save Changes" class="btn btn-primary mt-3" >                                             
                                </td>
                            </tr>

                            </form>

                        </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>


    </div>