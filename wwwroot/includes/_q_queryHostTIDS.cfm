
<!-- BEGIN #content -->
<div id="content" class="app-content">
    <cfif not(isdefined("TIDS"))>
        <h3>Query Host ID's</h1>
        <p>This tool was built to get the current data from the host. It does not do any post-processing, it simply brings raw data, parses and displays in a friendly format to troubleshoot any specific issues

        <cfoutput>
        <form name="check" method="post" action="index.cfm?action=#action#">
            <div class="row">
                <div class="cols-12">
                    <textarea class="form-control" name="TIDS"  rows="15"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="cols-12 float-right">
                    <input type="submit" class="btn btn-primary form-control" value="Validate">
                </div>
            </div>
        </form>
        </cfoutput>
    <cfelse>
        <cfset new = chr(13) & chr(10)>
        <cfset myString = replacenocase(TIDS,",",new,'ALL')>
        <cfoutput>

            <cfloop list="#myString#" delimiters="#new#" index="T">
                <h1>Transaction_ID: #T#</h1>
                <cfset TID = Application.admin.getTX(trim(T),'raw_cash')>

                <cfloop from="1" to="#TID.raw_cash.recordcount#" index="R">
                    <table class="table-responsive table-styled table-striped table">
                        <thead><tr><th colspan="2">#R#</th></tr></thead>
                        <tbody>
                        <cfloop list="#TID.raw_cash.columnlist#" index="C">
                            <tr>
                                <th>#c#</th>
                                <td>#evaluate("tid.raw_cash." & C)#</td>
                            </tr>
                        </cfloop>
                        </tbody>
                        <tfoot><tr><th colspan="2">api_cashtransaction</th></tr></tfoot>
                    </table>
                    <hr size="1">
                </cfloop>                    
            </cfloop>    
        </cfoutput>
    </cfif>    
</div>



<!---
b8a51201-3cc6-4d53-bb3a-09e3f65300e6
eddf7eb9-1b0a-4546-b5c4-813561a2e2c2
286c1e3e-be69-4cbf-a8e1-731f1649939a
6e856489-c805-486d-9455-1f9de97e57a5
3ef9e1e1-6746-4c02-b254-7a79e6c250b7
0367a453-9526-485b-995f-398f0c2f7672
03ad13f9-8529-4fa6-8f90-27a737b26bfe
e0a9f694-d209-4570-a8c9-2ae16c25ad01
0b4db933-da0b-402a-8df7-b1cab75d8e0c
84a17e7a-723a-4655-a64d-206bfe896b3c
2aff9f0e-6677-41c5-bb88-6252b7ca7b09
a7cdfa61-b250-4bf8-8785-fe88ba45aa6c
fb57e78f-55ec-4a3c-8355-ed8b5004e283
b059b38a-a3d2-459c-8b19-abca534026fc
e7be97ab-8f0c-43df-8c84-013a796aec99
b205ba02-dcb4-452e-9ab3-cc9deb06aa1c
4ef1d36a-0491-4e5d-af71-07aa0a16484f
ba6de28d-77ab-4ac1-a9fa-4d8a54ace795
b7fef748-30d6-4ed0-be6d-ab056883d304
92c9f314-0685-434c-a24e-c467838a6720
7b0fb0b5-69f0-4b94-8934-4f90ee5cd724
b698251d-94b6-43a0-aaae-ec3ffacfacc8
578c8178-175f-4177-9989-2e0db3bcca33
4ba6a0b8-128f-4a13-9ece-fe42c1a60058
2715b108-ab50-402f-bde9-74ccba4d30ba
7555cca8-58d5-45dc-a9d7-4921f2e7278b
57bad7e8-4552-4daa-8207-703e32f386e6
--->


