<!--- PERMIT --->
<cfset Permit = "Dashboards.Processor"><cfset hasPermit = Application.admin.hasPermit(session.userID,Permit)>
<cfif Not(hasPermit)><script>alert("Permission Denied");location.href="index.cfm";</script><cfabort></cfif>

<cfparam name="qYear" default="#year(now())#">

<cfset Stats = Application.admin.getStats(qYear,session.onlyNet,session.isForLocal)>

<!-- BEGIN #content -->
<div id="content" class="app-content">
	<!-- BEGIN row -->
    <div class="row">
        <div class="col-12">

            <div class="float-end">
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" name="isforlocal" id="isforLocal" <cfif session.isForLocal> checked </cfif>>
                    <label class="form-check-label" for="isforLocal">Is For Local Merchant</label>
                </div>
                <div class="form-check form-switch">
                    <input type="checkbox" class="form-check-input" name="onlyNet" id="onlyNet" <cfif session.onlyNet> checked </cfif>>
                    <label class="form-check-label" for="onlyNet">Only Net Transactions</label>
                </div>
    
            </div>

<h1>Processing Summary <cfoutput>#Qyear#</cfoutput></h1>

<cfquery dbtype="query" name="types">
    select distinct mytype from stats order by priority asc 
</cfquery>

<cfloop query="types">
    <cfquery dbtype="query" name="data">
        select * from stats where mytype = <cfqueryparam value="#myType#">
    </cfquery>


    <table class="styled-table table">
    <thead>
        <cfswitch expression="#myType#">
            <cfcase value="Processors">
                <tr><th class="width-20">Processor</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="3ds">
                <tr><th class="width-20">Trigger3DS</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="Issuers">
                <tr><th class="width-20">Issuer</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="Ecis">
                <tr><th class="width-20">Trigger3DS</th><th class="width-20">Mapped_ECI</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="cardtype">
                <tr><th class="width-20">card_Type</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="submissionChannel">
                <tr><th class="width-20">submission_Channel</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="fraudSuccess">
                <tr><th class="width-20">Fraud_success</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="isMsi">
                <tr><th class="width-20">isMSI</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="hasToken">
                <tr><th class="width-20">hasToken</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
            <cfcase value="isForLocalMerchant">
                <tr><th class="width-20">is_for_Local_Merchant</th><th class="width-20">Approved_Declined</th><th class="width-20">Total</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
            </cfcase>
        </cfswitch>
    </thead>
    <tbody>
        <cfoutput>
        <cfloop query="data">
            <tr>
            <td class="width-20">
                <cfswitch expression="#myType#">
                    <cfcase value="Processors">
                        <cfswitch expression="#myFilter1#">
                            <cfcase value="1">Openpay(1)</cfcase>
                            <cfcase value="2">Stripe(2)</cfcase>
                            <cfcase value="4">Banorte(4)</cfcase>
                            <cfdefaultcase>#myFilter1#</cfdefaultcase>
                        </cfswitch>
                    </cfcase>
                    <cfcase value="3ds,ecis,isMSI,fraudSuccess,hasToken,isForLocalMerchant">
                        <cfswitch expression="#myFilter1#">
                            <cfcase value="1">True</cfcase>
                            <cfcase value="0">False</cfcase>
                            <cfdefaultcase>#myFilter1#</cfdefaultcase>
                        </cfswitch>
                    </cfcase>
                    <cfcase value="cardType">
                        <cfswitch expression="#myFilter1#">
                            <cfcase value="C">Credit</cfcase>
                            <cfcase value="D">Debit</cfcase>
                            <cfdefaultcase>#myFilter1#</cfdefaultcase>
                        </cfswitch>
                    </cfcase>
                    <cfdefaultcase><cfif len(myFilter1) eq 0> N/A<cfelse>#myFilter1#</cfif></cfdefaultcase>
                </cfswitch>
            </td>
            <td class="width-20">
                <cfswitch expression="#myType#">
                    <cfcase value="Processors,3ds,Issuers,cardType,submissionChannel,isMsi,hasToken,fraudSuccess,isForLocalMerchant">
                        <cfswitch expression="#myFilter2#">
                            <cfcase value="A">Approved</cfcase>
                            <cfcase value="D">Declined</cfcase>
                            <cfdefaultcase>#myFilter2#</cfdefaultcase>
                        </cfswitch>
                    </cfcase>
                    <cfcase value="ecis">
                        <cfswitch expression="#myFilter2#">
                            <cfcase value="5">Approved (5)</cfcase>
                            <cfcase value="-1">Approved (-1)</cfcase>
                            <cfdefaultcase>#myFilter2#</cfdefaultcase>
                        </cfswitch>
                    </cfcase>
                    <cfdefaultcase>#myFilter2#</cfdefaultcase>
                </cfswitch>
            </td>
            <td class="width-20">#numberformat(myvalue1,'0,')#</td>
            <td class="width-20">$#numberformat(myvalue2,'0,')#</td>
            <td class="width-20">#numberformat(myPct1,'0.0')#%</td></tr>
        </cfloop>
        </cfoutput>
    </tbody>
    </table>
</cfloop>

</div>
</div>
</div>