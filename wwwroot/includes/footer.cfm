<cfparam name="attributes.action" default="dashboard">

<script src="/hud/assets/js/vendor.min.js"></script>
<script src="/hud/assets/js/app.min.js"></script>


        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready( function () {
                $('#dataTable').DataTable({
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],                    
                    "pageLength": 50,
                    "recordsTotal": 50
                    <cfif attributes.action eq 'otherpage'>
                        ,"order": [[ 1, "asc" ]]                    
                    <cfelseif listfindnocase("dashboard,search",attributes.action)>
                        ,"order": [[ 0, "desc" ]]                    
                    </cfif>
                    }
                );


                $('#isforLocal').click(function() {
                    if($('#isforLocal').is(':checked')) 
                        { location.href = "/index.cfm?action=<cfoutput>#attributes.action#</cfoutput>&isForLocal=1"; }
                    else
                        { location.href = "/index.cfm?action=<cfoutput>#attributes.action#</cfoutput>&isForLocal=0"; }
                });

                $('#onlyNet').click(function() {
                    if($('#onlyNet').is(':checked')) 
                        { location.href = "/index.cfm?action=<cfoutput>#attributes.action#</cfoutput>&onlyNet=1"; }
                    else
                        { location.href = "/index.cfm?action=<cfoutput>#attributes.action#</cfoutput>&onlyNet=0"; }
                });


            } );
        </script>

