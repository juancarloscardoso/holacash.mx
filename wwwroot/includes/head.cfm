<cfparam name="attributes.action" default="dashboard">

<cfoutput>
<head>
	<meta charset="utf-8" />
	<cfswitch expression="#attributes.action#">
		<cfcase value="dashboard"><title>HolaCash Transaction Summary</title></cfcase>
	</cfswitch>

	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	
	<!-- ================== BEGIN core-css ================== -->
	<link href="/hud/assets/css/vendor.min.css" rel="stylesheet" />
	<link href="/hud/assets/css/app.min.css" rel="stylesheet" />
	<link href="/includes/style.css?uuid=<cfoutput>#createuuid()#</cfoutput>" rel="stylesheet" />
	<!-- ================== END core-css ================== -->

	<cfswitch expression="#attributes.action#">
		<cfcase value="dashboard">
			<cfif not(isdefined("tracing_id"))>
				<meta http-equiv="refresh" content="60">			
			</cfif>
		</cfcase>
	</cfswitch>

</head>
</cfoutput>

