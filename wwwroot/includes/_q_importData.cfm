<h1>Data Import</h1>

<cfparam name="qmonth" default="#month(now())#">
<cfparam name="qyear" default="#year(now())#">

<cfset admin = createObject("component","/admin/admin").init()>
<cfif not(isdefined("step"))>
    <cfset Missing = Application.admin.missingSteps()>
<cfelse>
    <cfset Missing = "step" & step>
</cfif>


<cfset result = 0>
<cfswitch expression="#listfirst(Missing,':')#">
    <cfcase value="step2">
        <cfset result = Application.admin.import_TX_Date()>
    </cfcase>
    <cfcase value="step3">
        <cfset result = Application.admin.import_TX_Approve()>
    </cfcase>
    <cfcase value="step4">
        <cfset result = Application.admin.import_TX_Processor()>
    </cfcase>
    <cfcase value="step5">
        <cfset result = Application.admin.import_TX_3DS()>
    </cfcase>
    <cfcase value="step6">
        <cfset result = Application.admin.import_TX_Eci()>
    </cfcase>
    <cfcase value="step7">
        <cfset result = Application.admin.import_TX_Bin()>
    </cfcase>
    <cfcase value="step8">
        <cfset result = Application.admin.import_TX_Decline()>
    </cfcase>
    <cfcase value="step9">
        <cfset result = Application.admin.import_TX_MSI()>
    </cfcase>
    <cfcase value="step10">
        <cfset result = Application.admin.import_TX_Fraud()>
    </cfcase>
    <cfcase value="step11">
        <cfset result = Application.admin.import_TX_Description()>
    </cfcase>
    <cfcase value="step12">
        <cfset result = Application.admin.import_TX_Response()>
    </cfcase>
    <cfdefaultcase>
        <!--- step1 --->
        <!--- compare records by date and decide date --->
        <cfset result = Application.admin.import_TX_Core(qmonth,qyear)>
    </cfdefaultcase> 
</cfswitch>

<cfthread name="async1">
    <cfset result = Application.admin.import_TX_refunds()> <!--- Refunds --->
</cfthread>

<cfoutput>Step: #listfirst(Missing,':')# / Month: #Qmonth# / Year: #QYear#</cfoutput>
<br>done..
