<cfswitch expression="#type#">
    <cfcase value="Acceptance">
        <cfset Data = Application.admin.getAcceptance(session.onlyNet,filterID,qyear,qmonth,approveDecline,session.isForLocal)>
    </cfcase>
    <cfcase value="Declines">
        <cfset Data = Application.admin.getDeclines(session.onlyNet,filterID,qyear,qmonth,pec,session.isForLocal)>
    </cfcase>
</cfswitch>

<div id="content" class="app-content">
    <div class="row">
        <div class="col-12">
            <table class="table cell-border compact stripe table-responsive" id="dataTable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Tracing</th>
                        <th>Merchant</th>
                        <th>Amount</th>
                        <th>Bin</th>
                        <th>Status</th>
                        <th>Response</th>
                    </tr>
                </thead>
                <tbody>
                <cfoutput query="Data">
                    <tr>
                        <td>#dateformat(Date_created,'mm-dd')# #timeformat(Date_created,'HH:MM')#</td>
                        <td><a href="index.cfm?action=dashboard&tracing_id=#tracing_id#" class="btn btn-outline-theme me-2">Detail</a></td>
                        <td>#merchant_name#</td>
                        <td>#numberformat(amount,'0,.0')#</td>
                        <td>#card_number#</td>
                        <td><cfif Approved_Declined eq 'A'><span class="text-success">Approved</span><cfelse><span class="text-danger">Declined</span></cfif></td>
                        <td>#cash_decline_code#</td>
                    </tr>
                </cfoutput>
                </tbody>    
            </table>
        </div>
    </div>
</div>