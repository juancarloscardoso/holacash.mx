<!--- PERMIT --->
<cfset Permit = "Settings.Setup"><cfset hasPermit = Application.admin.hasPermit(session.userID,Permit)>
<cfif Not(hasPermit)><script>alert("Permission Denied");location.href="index.cfm";</script><cfabort></cfif>

<!-- BEGIN #content -->
<div id="content" class="app-content">

    <cfif isdefined("form.update")>
        <cfswitch expression="#form.update#">
            <cfcase value="cash_decline_code">
                <cfset result = Application.admin.saveSettings('cashDeclineCodes',form)>
            </cfcase>
        </cfswitch>
    </cfif>
    
    <cfset errors = Application.admin.getErrorCodes()>
    <cfset merchants = Application.admin.getMerchants()>
    


    <p>The following allows Dashboards to filter between <b><span class="text-success">NET</span> & GROSS</b> transactions. <span class="text-success"><B>NET</B></span> transactions DO NOT include any error codes that are outside of our control or any merchants that are test merchants.</p>


	<!-- BEGIN row -->
    <div class="row">
        <div class="col-2">
            &nbsp;
        </div>
        <div class="col-10">

            <h3>Cash_Decline_Codes</h3>
            <table class="table table-styled">
                <tbody>
                    <cfoutput query="errors">
                        <cfif cash_decline_code neq 0>
                            <form name="actualiza" method="post" action="index.cfm?action=settings">
                                <input type="hidden" name="update" value="cash_decline_code">
                                <input type="hidden" name="cash_decline_code" value="#trim(cash_decline_code)#">
                            <tr>
                                <td>
                                    <h5><cfif len(trim(cash_decline_code)) eq 0><i>null</i><cfelse>#trim(cash_decline_code)#</cfif></h5>
                                </td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input type="checkbox" class="form-check-input" name="cash_switch" id="customSwitch1" <cfif listfindnocase(Application.admin.get_nonNetCashErrorCodes('list'),trim(cash_decline_code))>  checked </cfif> >
                                        <label class="form-check-label" for="customSwitch1"> <b><cfif len(trim(cash_decline_code)) eq 0>null<cfelse>#cash_decline_code#</cfif></b> - Is Net Transaction</label>
                                    </div>
                                    <input type="text" name="cash_label" required value="#cash_label#" class="form-control">
                                    <br>
                                </td>
                                <td>
                                    <input type="submit" value="Update" class="btn btn-primary">
                                </td>
                            </tr>
                            </form>                            
                        </cfif>

                    </cfoutput> 

                </tbody>

            </table>



        </div>
    </div>

    <p style="margin-top: 50px;">&nbsp;</p>

    <div class="row">
        <div class="col-2">
            &nbsp;
        </div>
        <div class="col-10">

            <h3>Test Merchants</h3>

            <table class="styled-table table">
                <tbody>
                    <cfoutput query="merchants">
                        <cfif msid neq 0>
                            <form name="actualiza" method="post" action="index.cfm?action=settings">
                                <input type="hidden" name="update" value="merchants">

                            <tr>
                                <td><h5><cfif len(merchant_name) eq 0><i>null</i><cfelse>#merchant_name#</cfif></h5></td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input type="checkbox" class="form-check-input" name="cash_switch" id="customSwitch1" <cfif listfindnocase(Application.admin.get_testMerchants('list'),trim(MSID))>  checked </cfif> >
                                        <label class="form-check-label" for="customSwitch1"> <b>#merchant_name#</b> - Is Test Merchant</label>
                                    </div>
                                </td>
                                <td>
                                    <input type="submit" value="Update" class="btn btn-primary">
                                </td>
                            </tr>
                            </form>
                        </cfif>                    
                    </cfoutput>
                </tbody>
            </table>
        </div>

    </div>


</div>