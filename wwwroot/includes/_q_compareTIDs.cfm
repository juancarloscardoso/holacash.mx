<!-- BEGIN #content -->
<div id="content" class="app-content">
    <cfif not(isdefined("TIDS"))>
        <h3>Compare Tracing ID's</h1>
        <p>This tool was built for audit purposes. It looks for a given list of Tracing_IDS and returns Results to validate their status and existance.
            Capture the Tracing_IDS you wish to review separated by rows or commas.
        </p>

        <cfoutput>
        <form name="check" method="post" action="index.cfm?action=#action#">
            <div class="row">
                <div class="cols-12">
                    <textarea class="form-control" name="TIDS"  rows="15"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="cols-12 float-right">
                    <input type="submit" class="btn btn-primary form-control" value="Validate">
                </div>
            </div>
        </form>
        </cfoutput>
    <cfelse>
        <cfset new = chr(13) & chr(10)>
        <cfset myString = replacenocase(TIDS,",",new,'ALL')>
        <cfoutput>
        <h3>Analyzed #listlen(myString,new)# Transaction_ID's</h3>

        <table class="table table-styled table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Tracing</th>
                    <th>isNet</th>
                    <th>IsLocal</th>
                    <th>Merchant</th>
                    <th>Amount</th>
                    <th>Bin</th>
                    <th>Status</th>
                    <th>Response</th>
                </tr>
        </thead>

            <cfloop list="#myString#" delimiters="#new#" index="T">
                <cfset TID = Application.admin.getTX(trim(T),'core')>
                <cfif TID.Core.recordcount eq 1>
                    <cfset Ready = Application.admin.isDataReady(trim(T))>

                    <!--- Same format as Dashboard  --->
                    <tr>
                        <td>#listfindnocase(myString,T,new)#</td>
                        <td>#dateformat(tid.core.Date_created,'mm-dd')# #timeformat(tid.core.Date_created,'HH:MM')#</td>
                            <td><a href="index.cfm?action=dashboard&tracing_id=#tid.core.tracing_id#" class="btn btn-outline-theme me-2" <cfif not(ready)> disabled</cfif>>Detail <cfif not(ready)>(...)</cfif></a></td>
                        <td>
                            <cfif booleanFormat(Application.admin.isNetTX(tid.core.tracing_id))>
                                <span class="text-success">Yes</span>
                            <cfelse>
                                <span class="text-danger">No</span>
                            </cfif>
                        </td>
                        <td>
                            <cfif tid.core.is_for_local_merchant>
                                <span class="text-success">Yes</span>
                            <cfelse>
                                <span class="text-danger">No</span>
                            </cfif>
                        </td>                            
                        <td>#tid.core.merchant_name#</td>
                        <td>#numberformat(tid.core.amount,'0,.0')#</td>
                        <td>#tid.core.card_number#</td>
                        <td><cfif tid.core.Approved_Declined eq 'A'><span class="text-success">Approved</span><cfelse><span class="text-danger">Declined</span></cfif></td>
                        <td><cfif tid.core.cash_decline_code eq Application.admin.getErrorCodeMeaning(tid.core.cash_decline_code)>#tid.core.cash_decline_code#<cfelse>#tid.core.cash_decline_code# - #Application.admin.getErrorCodeMeaning(tid.core.cash_decline_code)#</cfif></td>
                    </tr>
               <cfelse>
                    <TR>
                        <td colspan="10">#T# not found in Database!</td>
                    </TR>
                </cfif>
            </cfloop>
        </table>

        </cfoutput>
    </cfif>    
</div>



<!---
b8a51201-3cc6-4d53-bb3a-09e3f65300e6
eddf7eb9-1b0a-4546-b5c4-813561a2e2c2
286c1e3e-be69-4cbf-a8e1-731f1649939a
6e856489-c805-486d-9455-1f9de97e57a5
3ef9e1e1-6746-4c02-b254-7a79e6c250b7
0367a453-9526-485b-995f-398f0c2f7672
03ad13f9-8529-4fa6-8f90-27a737b26bfe
e0a9f694-d209-4570-a8c9-2ae16c25ad01
0b4db933-da0b-402a-8df7-b1cab75d8e0c
84a17e7a-723a-4655-a64d-206bfe896b3c
2aff9f0e-6677-41c5-bb88-6252b7ca7b09
a7cdfa61-b250-4bf8-8785-fe88ba45aa6c
fb57e78f-55ec-4a3c-8355-ed8b5004e283
b059b38a-a3d2-459c-8b19-abca534026fc
e7be97ab-8f0c-43df-8c84-013a796aec99
b205ba02-dcb4-452e-9ab3-cc9deb06aa1c
4ef1d36a-0491-4e5d-af71-07aa0a16484f
ba6de28d-77ab-4ac1-a9fa-4d8a54ace795
b7fef748-30d6-4ed0-be6d-ab056883d304
92c9f314-0685-434c-a24e-c467838a6720
7b0fb0b5-69f0-4b94-8934-4f90ee5cd724
b698251d-94b6-43a0-aaae-ec3ffacfacc8
578c8178-175f-4177-9989-2e0db3bcca33
4ba6a0b8-128f-4a13-9ece-fe42c1a60058
2715b108-ab50-402f-bde9-74ccba4d30ba
7555cca8-58d5-45dc-a9d7-4921f2e7278b
57bad7e8-4552-4daa-8207-703e32f386e6
--->


