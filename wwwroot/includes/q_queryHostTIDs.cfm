<!-- BEGIN #content -->
<div id="content" class="app-content">
    <cfif not(isdefined("TIDS"))>
        <h3>Query Host Tracing ID's</h1>
        <p>This tool was built to get the current data from the host. It does not do any post-processing, it simply brings raw data, parses and displays in a friendly format to troubleshoot any specific issues
        </p>

        <cfoutput>
        <form name="check" method="post" action="index.cfm?action=#action#">
            <div class="row">
                <div class="cols-12">
                    <label>Tracing ID or Transaction ID:</label>
                    <input type="text" class="form-control" name="TIDS"  rows="15">
                </div>
            </div>
            <div class="row">
                <div class="cols-12 float-right">
                    <input type="submit" class="btn btn-primary form-control" value="Validate">
                </div>
            </div>
        </form>
        </cfoutput>
    <cfelse>
        <cfset new = chr(13) & chr(10)>
        <cfset myString = replacenocase(TIDS,",",new,'ALL')>
        <cfoutput>
        <h3>Analyzed #listlen(myString,new)# Transaction_ID's</h3>

        <table class="table table-styled table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Tracing</th>
                    <th>isNet</th>
                    <th>IsLocal</th>
                    <th>Merchant</th>
                    <th>Amount</th>
                    <th>Bin</th>
                    <th>Status</th>
                    <th>Response</th>
                </tr>
        </thead>

            <cfloop list="#myString#" delimiters="#new#" index="T">
                <cfset TID = Application.admin.getTX(trim(T),'hola')>
                <cfif TID.Core.recordcount eq 1> 
                    <cfset Ready = Application.admin.isDataReady(trim(T))>

                    <!--- Same format as Dashboard  --->
                    <tr>
                        <td>#listfindnocase(myString,T,new)#</td>
                        <td>#dateformat(tid.core.Date_created,'mm-dd')# #timeformat(tid.core.Date_created,'HH:MM')#</td>
                            <td><a href="index.cfm?action=dashboard&tracing_id=#tid.core.tracing_id#" class="btn btn-outline-theme me-2" <cfif not(ready)> disabled</cfif>>Detail <cfif not(ready)>(...)</cfif></a></td>
                        <td>
                            <cfif booleanFormat(Application.admin.isNetTX(tid.core.tracing_id))>
                                <span class="text-success">Yes</span>
                            <cfelse>
                                <span class="text-danger">No</span>
                            </cfif>
                        </td>
                        <td>
                            <cfif tid.core.is_for_local_merchant>
                                <span class="text-success">Yes</span>
                            <cfelse>
                                <span class="text-danger">No</span>
                            </cfif>
                        </td>                            
                        <td>#tid.core.merchant_name#</td>
                        <td>#numberformat(tid.core.amount,'0,.0')#</td>
                        <td>#tid.core.card_number#</td>
                        <td><cfif tid.core.Approved_Declined eq 'A'><span class="text-success">Approved</span><cfelse><span class="text-danger">Declined</span></cfif></td>
                        <td><cfif tid.core.cash_decline_code eq Application.admin.getErrorCodeMeaning(tid.core.cash_decline_code)>#tid.core.cash_decline_code#<cfelse>#tid.core.cash_decline_code# - #Application.admin.getErrorCodeMeaning(tid.core.cash_decline_code)#</cfif></td>
                    </tr>
               <cfelse>
                    <TR>
                        <td colspan="10">#T# not found in Database!</td>
                    </TR>
                </cfif>
            </cfloop>
        </table>

        </cfoutput>
    </cfif>    
</div>
