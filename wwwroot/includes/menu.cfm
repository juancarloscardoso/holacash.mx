
    <div class="menu">
        <div class="menu-header">Data-Sync at: <cfoutput>#Application.admin.dataSyncPercent()#</cfoutput></div>
        <div class="menu-item  <cfif attributes.action eq 'dashboard'>active</cfif>">
            <a href="index.cfm?action=Dashboard" class="menu-link">
                <span class="menu-icon"><i class="bi bi-cpu"></i></span>
                <span class="menu-text">Inicio</span>
            </a>
        </div>
        <div class="menu-item has-sub <cfif listfindnocase('acceptance,declines,processor',attributes.action)>active</cfif>">
            <a href="#" class="menu-link">
                <span class="menu-icon">
                    <i class="bi bi-eye"></i>
                </span>
                <span class="menu-text">Dashboards</span>
                <span class="menu-caret"><b class="caret"></b></span>
            </a>
            <div class="menu-submenu" >
                <div class="menu-item <cfif attributes.action eq 'acceptance'>active</cfif>">
                    <a href="index.cfm?action=acceptance" class="menu-link">
                        <span class="menu-text">Acceptance</span>
                    </a>
                </div>

                <div class="menu-item  <cfif attributes.action eq 'declines'>active</cfif>">
                    <a href="index.cfm?action=declines" class="menu-link">
                        <span class="menu-text">Declines</span>
                    </a>
                </div>

                <div class="menu-item <cfif attributes.action eq 'processor'>active</cfif>">
                    <a href="index.cfm?action=processor" class="menu-link">
                        <span class="menu-text">Processor</span>
                    </a>
                </div>

                <div class="menu-item <cfif attributes.action eq 'download'>active</cfif>">
                    <a href="index.cfm?action=download" class="menu-link">
                        <span class="menu-text">Download</span>
                    </a>
                </div>

            </div>
        </div>


        <div class="menu-item has-sub <cfif listfindnocase('settings,users,permits',attributes.action)>active</cfif>">
            <a href="#" class="menu-link">
                <span class="menu-icon">
                    <i class="bi bi-eye"></i>
                </span>
                <span class="menu-text">Settings</span>
                <span class="menu-caret"><b class="caret"></b></span>
            </a>
            <div class="menu-submenu" >
                <div class="menu-item <cfif attributes.action eq 'settings'>active</cfif>">
                    <a href="index.cfm?action=settings" class="menu-link">
                        <span class="menu-text">Setup</span>
                    </a>
                </div>

                <div class="menu-item  <cfif attributes.action eq 'users'>active</cfif>">
                    <a href="index.cfm?action=users" class="menu-link">
                        <span class="menu-text">Users</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="menu-item has-sub <cfif listfindnocase('q_compareTIDS',attributes.action)>active</cfif>">
            <a href="#" class="menu-link">
                <span class="menu-icon">
                    <i class="bi bi-eye"></i>
                </span>
                <span class="menu-text">Tools</span>
                <span class="menu-caret"><b class="caret"></b></span>
            </a>
            <div class="menu-submenu" >
                <div class="menu-item <cfif attributes.action eq 'q_importData'>active</cfif>">
                    
                    <a href="index.cfm?action=q_importData" class="menu-link">
                        <span class="menu-text">Import Data</span>
                    </a>
                </div>

                <div class="menu-item <cfif attributes.action eq 'q_compareTIDS'>active</cfif>">
                    <a href="index.cfm?action=q_compareTIDS" class="menu-link">
                        <span class="menu-text">Compare TIDS</span>
                    </a>
                </div>
                <div class="menu-item <cfif attributes.action eq 'q_queryHostTIDS'>active</cfif>">
                    <a href="index.cfm?action=q_queryHostTIDS" class="menu-link">
                        <span class="menu-text">Query Host</span>
                    </a>
                </div>
            </div>


        </div>


    </div>
    <!-- END menu -->