<!--- PERMIT --->
<cfset Permit = "Dashboards.Declines"><cfset hasPermit = Application.admin.hasPermit(session.userID,Permit)>
<cfif Not(hasPermit)><script>alert("Permission Denied");location.href="index.cfm";</script><cfabort></cfif>

<!-- BEGIN #content -->
<div id="content" class="app-content">
	<!-- BEGIN row -->
    <div class="row">
        <div class="col-12">
    
    
    
    <cfset Declines = Application.admin.getDeclines(session.onlyNet,0,year(now()),0,0,session.isForLocal)>
    <cfset errorCodes = Application.admin.getErrorCodes()>
    
        <cfparam name="qYear" default="#year(now())#">

        <div class="float-end">
            <div class="form-check form-switch">
                <input type="checkbox" class="form-check-input" name="isforlocal" id="isforLocal" <cfif session.isForLocal> checked </cfif>>
                <label class="form-check-label" for="isforLocal">Is For Local Merchant</label>
            </div>
            <div class="form-check form-switch">
                <input type="checkbox" class="form-check-input" name="onlyNet" id="onlyNet" <cfif session.onlyNet> checked </cfif>>
                <label class="form-check-label" for="onlyNet">Only Net Transactions</label>
            </div>

        </div>


        <h1>Declines Summary <cfoutput>#Qyear#</cfoutput></h1>
        <h5>- <cfif session.onlyNet eq 0><a href="index.cfm?action=declines&onlyNet=1">All Transactions (click for net)</a><cfelse><a href="index.cfm?action=declines&onlyNet=0">Net Transactions (click for all)</a></cfif> -</h5>
        
    <cfoutput>
    
            <cfloop query="errorCodes">
                <cfquery dbtype="query" name="data">
                    select * from Declines where cash_decline_code = <cfqueryparam value="#cash_decline_code#">
                </cfquery>
    
                <cfif data.total gt 0 and len(cash_decline_code) gt 0>
                    <cfif data.cdc neq 0>
                        <br><h3>#data.cash_decline_code# <cfif data.cdc neq data.cash_decline_code>- #data.cdc#</cfif></h3>
                    </cfif>
    
                    <table class="table table-styled">
                        <thead>
                            <tr><th class="width-20">Processor_Decline_Code</th><th class="width-20">Month</th><th class="width-20">Txs</th><th class="width-20">Amount</th><th class="width-20">Percent</th></tr>
                        </thead>
    
                        <cfset tempSum = structnew()>
                        <cfset tempSum["total"] = 0>
                        <cfset tempSum["monto"] = 0>
                        <cfset tempSum["totalPCT"] = arraynew(1)>
    
      
                        <cfloop query="data">
                            <cfif cdc neq 0>
                                <cfset tempSum["total"] = tempSum.total + total>
                                <cfset tempSum["monto"] = tempSum.monto + monto>
                                <cfset arrayappend(tempSum.totalPCT,totalPCT)>
                            </cfif>    
        
                                <tbody>
                                    <tr>
                                        <td class="width-20"><cfif cdc eq 0>ALL CODES<cfelse>#processor_decline_code# - 
                                            #Application.admin.getProcessorName(processor_id)#    
                                            <br>#processor_decline_message#                                      
                                        </cfif></td>
                                        <td class="left-border">#monthasString(myMonth)#</td>
                                        <cfif total gt 0>
                                            <td class="left-border"><a class="btn btn-sm btn-primary" href="index.cfm?action=detail&filterId=#trim(cash_decline_code)#&type=Declines&onlyNet=#session.onlyNet#&qMonth=#MyMonth#&qyear=#myYear#&PEC=#Processor_Decline_Code#">#numberformat(Total,'0,')#</a></td>
                                        <cfelse>
                                            <td class="left-border">#numberformat(Total,'0,')#</td>
                                        </cfif>
                                        <td class="width-20">$#numberformat(Monto,'0,')#</td>
                                        <td class="left-border">#numberformat(TotalPct,'0,.00')#%</td>
                                    </tr>
                                </tbody> 
                        </cfloop>   
    
                        <tfoot>
                            <tr>
                                <td colspan=2>YTD</td>
                                <td class="left-border">#numberformat(tempsum.total,'0,')#</td>
                                <td class="width-20">$#numberformat(tempsum.Monto,'0,')#</td>
                                <td class="width-20">#numberformat(arrayavg(tempsum.totalPCT),'0.00')#%</th>
                            </tr>
                            </tfoot>
            
    
                    </table>
                </cfif>            
    
    
            </cfloop>
    
    </cfoutput>
    

</div>
</div>
</div>
    