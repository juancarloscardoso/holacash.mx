<cfcomponent>
    <cffunction name="init" hint="initializes component with Credentials">
        <cfset this["holacash"] = structnew()>
        <cfset this.holacash["privateKey"] = "skt_sandbox_UsmSPOJS.nXgVqZrwx7O91JCy3tBw9e0RDTDXsNn6">
        <cfset this.holacash["publicKey"] = "pub_sandbox_i8cC3JCh.RUHCeryfjEA9FWgVtiQwSHdT35z51WGK">
        <cfset this.holacash["endpoint"] = "https://sandbox.api.holacash.mx/v2">
        <!--- Matriz Prueba Ops --->
        <cfset this.holacash["privateKey"] = "skt_live_EvseNdih.9kkeNWO0K8nKuQgvH0bPl5RuTSjLMWBl">
        <cfset this.holacash["endpoint"] = "https://live.api.holacash.mx/v2">

        <cfreturn this>
    </cffunction>



    <cffunction name="holacash_metaFraud" hint="returns Fraud X-meta data string for header: This information comes from Client App, for Dev/Backend use TestHeader (useTest)">
        <cfargument name="useTest" default="true">

        <cfif arguments.useTest>
            <cfset FraudMeta = "ewogICAgImlwX2FkZHJlc3MiOiIxMjMuMTIzLjEyMy4xMjMiLAogICAgImRldmljZV9pZCIgOiAiMTIzNDU2IiwKICAgICJ1c2VyX3RpbWV6b25lIiA6IiswMzozNCIsCiAgICAidXJsX2RvbWFpbl9uYW1lIiA6ImhvbGFjYXNoLm14IiwKICAgICJicm93c2VyX3ZlcnNpb24iIDoiMTIzIiwKICAgICJicm93c2VyX25hbWUiIDoiTW96aWxsYSIsCiAgICAiYnJvd3Nlcl90aW1lem9uZSIgOiJQU1QiLAogICAgInNjcmVlbl9yZXNvbHV0aW9uX3dpZHRoIiA6IjEyMyIsCiAgICAic2NyZWVuX3Jlc29sdXRpb25faGVpZ2h0IiA6IjEyMyIsCiAgICAid2luZG93X3Bvc2l0aW9uX3giIDoiMTIzIiwKICAgICJ3aW5kb3dfcG9zaXRpb25feSIgOiIxMjMiLAogICAgImNvbG9yX2RlcHRoIiA6IjEyMyIKfQ====">
        <cfelse>
            <cfset FraudMeta = {
                "ip_address" : "#cgi.REMOTE_ADDR#",
                "device_id" : "00000000-00000000-01234567-89ABCDEF",
                "user_timezone" : "#ToString(getTimeZone().utchouroffset)#"
                }>
            <cfset FraudMeta = tobase64(toString(serializeJSON(FraudMeta)))>

            <!----------------------------- FULL FRAUD HEADER, USE AS NEEDED -------------------------------------->
            <!--- Client App should Ideally provide the following information (default Is HolaCash's sample) ------>
            <!--- Fields with values are HolaCash's sample, required fields are only first three, rest optional --->
            <!--- Careful with datatypes. Most are strings and if sent as integers, dates or nulls will error ----->
            <!-----------------------------------------------------------------------------------------------------
                { 
                            "ip_address": "123.123.123.123", 
                            "device_id" : "123456", 
                        "user_timezone" : "+03:34", 
                      "url_domain_name" : "holacash.mx", 
                      "browser_version" : "123", 
                        "browser_name"  : "Mozilla",
                     "browser_timezone" : "PST",
              "screen_resolution_width" : "123", 
             "screen_resolution_height" : "123", 
                    "window_position_x" : "123", 
                    "window_position_y" : "123", 
                          "color_depth" : "123"  
                      "optional fields" : "", 
                    "user-agent-string" : "",
                     "is_rooted_device" : "",
                         "is_incognito" : "",
                    "location_latitude" : "",
                   "location_longitude" : "",
                           "os_version" : "",
                              "os_type" : "",
                          "phone_brand" : "",
                         "phone_carrier : "",
                         "battery_level : "",
                   "is_battery_charging : "",
                  "is_connected_to_wifi : "",
                           "pixel_depth : "",
                           "pixel_ratio : "",
                }
            --->
        </cfif>
        <cfreturn FraudMeta>
    </cffunction>

    <cffunction name="holacash_createToken" hint="returns token string or debug struct">
        <cfargument name="method" default="credit_or_debit_card" required=false>
        <cfargument name="card_number" default="4242424242424242">
        <cfargument name="expiration_month" default="12">
        <cfargument name="expiration_year" default="2034">
        <cfargument name="card_validation_code" default="123">
        <cfargument name="consumer_email" default="">
        <cfargument name="first_name" default="">
        <cfargument name="second_first_name" default="">
        <cfargument name="first_last_name" default="">
        <cfargument name="second_last_name" default="">
        <cfargument name="debug" default=false required=false>
        <cfargument name="useTest" default=false required=false>

        <cfset Payload = {
        "credential": 
        {
            "payment_method": { "method": "#arguments.method#" },
            "credit_or_debit_card": 
                {
                "card_number": "#arguments.card_number#",
                "expiration_month": "#arguments.expiration_month#",
                "expiration_year": "#arguments.expiration_year#",
                "card_validation_code": "#arguments.card_validation_code#"
                }
        },
            "consumer_details": 
                {
                "contact": { "email": "#arguments.consumer_email#" },
                "name": 
                    {
                    "first_name": "#arguments.first_name#",
                    "second_first_name": "#arguments.second_first_name#",
                    "first_last_name": "#arguments.first_last_name#",
                    "second_last_name": "#arguments.second_last_name#"
                    }
                }
        }>

        <cfhttp url="#this.holacash.endpoint#/tokenization/payment_token" method="POST">
            <cfhttpparam type="header" name="X-Api-Client-Key" value="#this.holacash.privateKey#" />
            <cfhttpparam type="header" name="X-Cash-Anti-Fraud-Metadata" value="#holacash_metaFraud(arguments.useTest)#" />
            <cfhttpparam type="header" name="Content-Type" value="application/json">                
            <cfhttpparam type="body" value="#serializeJson(Payload)#" />
        </cfhttp>

        <cftry>
            <cfif debug>
                <cfset Result = structnew()>
                <cfset Result["cfhttp"] = cfhttp>
                <cfset Result["payload"] = serializeJSON(Payload)>
                <cfset Result["xfraud"] = holacash_metaFraud(arguments.useTest)>
                <cfset Result["endpoint"] = "#this.holacash.endpoint#/tokenization/payment_token">
                <cfset Result["xapikey"] = this.holacash.privateKey>
                <cfreturn Result>
            <cfelse>
                <cfset temp = deserializeJSON(cfhttp.filecontent)>
                <cfif isdefined("temp.token_details")>
                    <cfreturn temp.token_details.token>
                <cfelse>
                    <cfreturn temp> 
                </cfif>
            </cfif>
            <cfcatch><cfreturn cfcatch></cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="holacash_newTokenPay" hint="uses a Token to process a payment">
        <cfargument name="token">
        <cfargument name="amount">
        <cfargument name="currency_code" default="MXN" required="false">
        <cfargument name="description">
        <cfargument name="debug" default=false required=false>

        <cfset PayLoad = 
        {
            "description": "#arguments.description#",
            "amount_details": {
                "amount": #arguments.amount#,
                "currency_code": "#arguments.currency_code#"
            },
            "payment_detail": {
                "credentials": {
                    "payment_method": {
                        "method": 'pay_with_holacash_payment_token'
                    },
                    "holacash_payment_token": {
                        "payment_token": "#arguments.token#"
                    }
                }
            },  
            "processing_instructions": {
                "auto_capture": true
            }
        }
        >

        <cfhttp url="#this.holacash.endpoint#/transaction/charge" method="POST">
            <cfhttpparam type="header" name="X-Api-Client-Key" value="#this.holacash.privateKey#" />
            <cfhttpparam type="header" name="X-Cash-Anti-Fraud-Metadata" value="#holacash_metaFraud()#" />
            <cfhttpparam type="header" name="Content-Type" value="application/json">                
            <cfhttpparam type="body" value="#serializeJson(Payload)#" />
        </cfhttp>

        <cftry>
            <cfif debug>
                <cfset Result = structnew()>
                <cfset Result["cfhttp"] = cfhttp>
                <cfset Result["payload"] = serializeJSON(Payload)>
                <cfset Result["xfraud"] = holacash_metaFraud()>
                <cfset Result["endpoint"] = "#this.holacash.endpoint#/transaction/charge">
                <cfset Result["xapikey"] = this.holacash.privateKey>
                <cfreturn Result>
            <cfelse>
                <cfset Result = structnew()>
                <cftry>
                    <cfset Result["id"] = deserializeJSON(cfhttp.filecontent).id>
                    <cfcatch></cfcatch>                    
                </cftry>
                <cfset Result["message"] = deserializeJSON(cfhttp.filecontent).status_details.message>
                <cfset Result["status"] = deserializeJSON(cfhttp.filecontent).status_details.status>
                <cfreturn Result>
            </cfif>
            <cfcatch><cfreturn cfcatch></cfcatch>
        </cftry>

    </cffunction>


    <cffunction name="holacash_charge" hint="creates a charge without need for token">
        <cfargument name="card_number" default="4242424242424242">
        <cfargument name="expiration_month" default="12">
        <cfargument name="expiration_year" default="2034">
        <cfargument name="card_validation_code" default="123">
        <cfargument name="consumer_email" default="">
        <cfargument name="id" default="">
        <cfargument name="first_name" default="">
        <cfargument name="second_first_name" default="">
        <cfargument name="first_last_name" default="">
        <cfargument name="second_last_name" default="">
        <cfargument name="debug" default=false required=false>
        <cfargument name="useTest" default=false required=false>
        <cfargument name="amount">
        <cfargument name="currency_code" default="MXN" required="false">
        <cfargument name="description">
        <cfargument name="autocapture" default=true required="false">


        <cfset PayLoad = 
            {
                "description": "#arguments.description#",
                "amount_details": {
                    "amount": #arguments.amount#,
                    "currency_code": "#arguments.currency_code#"
                },
                "payment_detail": {
                    "credentials": {
                        "payment_method": {
                            "method": "credit_or_debit_card"
                        },
                        "credit_or_debit_card": {
                            "card_number": "#arguments.card_number#",
                            "expiration_month": "#arguments.expiration_month#",
                            "expiration_year": "#arguments.expiration_year#",
                            "card_validation_code": "#arguments.card_validation_code#"
                        }
                    } 
                },
                "consumer_details": {
                    "external_consumer_id": "0",                    
                    "contact": {
                        "email": "#arguments.consumer_email#"
                    },
                    "name": {
                        "first_name": "#arguments.first_name#",
                        "second_first_name": "#arguments.second_first_name#",
                        "first_last_name": "#arguments.first_last_name#",
                        "second_last_name": "#arguments.second_last_name#"
                    }
                },
                "processing_instructions": {
                    "auto_capture": #arguments.autocapture#
                }
            }
            >


            <cfhttp url="#this.holacash.endpoint#/transaction/charge" method="POST">
                <cfhttpparam type="header" name="X-Api-Client-Key" value="#this.holacash.privateKey#" />
                <cfhttpparam type="header" name="X-Cash-Anti-Fraud-Metadata" value="#holacash_metaFraud()#" />
                <cfhttpparam type="header" name="Content-Type" value="application/json">                
                <cfhttpparam type="body" value="#serializeJson(Payload)#" />
            </cfhttp>
    
            <cftry>
                <cfif debug>
                    <cfset Result = structnew()>
                    <cfset Result["cfhttp"] = cfhttp>
                    <cfset Result["payload"] = serializeJSON(Payload)>
                    <cfset Result["xfraud"] = holacash_metaFraud()>
                    <cfset Result["endpoint"] = "#this.holacash.endpoint#/transaction/charge">
                    <cfset Result["xapikey"] = this.holacash.privateKey>
                    <cfreturn Result>
                <cfelse>
                    <cfset Result = structnew()>
                    <cftry>
                        <cfset Result["id"] = deserializeJSON(cfhttp.filecontent).id>
                        <cfcatch></cfcatch>                    
                    </cftry>
                    <cfset Result["message"] = deserializeJSON(cfhttp.filecontent).status_details.message>
                    <cfset Result["status"] = deserializeJSON(cfhttp.filecontent).status_details.status>
                    <cfreturn Result>
                </cfif>
                <cfcatch><cfreturn cfcatch></cfcatch>
            </cftry>
    
        </cffunction>



    <cffunction name="holacash_getTransaction" hint="returns info about a transaction">
        <cfargument name="transactionId">

        <cfhttp url="#this.holacash.endpoint#/transaction/charge/#arguments.transactionId#" method="GET">
            <cfhttpparam type="header" name="X-Api-Client-Key" value="#this.holacash.privateKey#" />
        </cfhttp>

        <cfreturn deserializeJSON(cfhttp.filecontent)>
    </cffunction> 
</cfcomponent>